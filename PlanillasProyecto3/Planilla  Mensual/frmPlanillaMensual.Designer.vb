﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPlanillaMensual
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim NoCEDULALabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim __ANUALLabel As System.Windows.Forms.Label
        Dim ANUALIDADLabel As System.Windows.Forms.Label
        Dim SALARIO_BRUTOLabel As System.Windows.Forms.Label
        Dim SALARIO_DIARIOLabel As System.Windows.Forms.Label
        Dim LIQUIDO_A_PAGARLabel As System.Windows.Forms.Label
        Dim DIAS_TRABAJLabel As System.Windows.Forms.Label
        Dim DiasIncapLabel As System.Windows.Forms.Label
        Dim InstitucionIncapacidadLabel As System.Windows.Forms.Label
        Dim HORAS_EXTRAS_REGULARLabel As System.Windows.Forms.Label
        Dim HORAS_EXTRAS_DOBLESLabel As System.Windows.Forms.Label
        Dim TOTAL_DIAS_INCAPLabel As System.Windows.Forms.Label
        Dim FechaINCAPINILabel As System.Windows.Forms.Label
        Dim FechaINCAPFINLabel As System.Windows.Forms.Label
        Dim AsientoLabel As System.Windows.Forms.Label
        Dim IMP_RENTALabel As System.Windows.Forms.Label
        Dim EXTRASLabel As System.Windows.Forms.Label
        Dim CESANTIALabel As System.Windows.Forms.Label
        Dim SUBSIDIOLabel As System.Windows.Forms.Label
        Dim CodDedLabel As System.Windows.Forms.Label
        Dim CCSSLabel As System.Windows.Forms.Label
        Dim FechaPlanMensualLabel1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPlanillaMensual))
        Me.Planilla2DataSet = New PlanillasProyecto3.Planilla2DataSet()
        Me.PlanillaMensualBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PlanillaMensualTableAdapter = New PlanillasProyecto3.Planilla2DataSetTableAdapters.PlanillaMensualTableAdapter()
        Me.TableAdapterManager = New PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager()
        Me.PlanillaMensualBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.PlanillaMensualBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.NoCEDULATextBox = New System.Windows.Forms.TextBox()
        Me.NombreTextBox = New System.Windows.Forms.TextBox()
        Me.__ANUALTextBox = New System.Windows.Forms.TextBox()
        Me.ANUALIDADTextBox = New System.Windows.Forms.TextBox()
        Me.SALARIO_BRUTOTextBox = New System.Windows.Forms.TextBox()
        Me.SALARIO_DIARIOTextBox = New System.Windows.Forms.TextBox()
        Me.LIQUIDO_A_PAGARTextBox = New System.Windows.Forms.TextBox()
        Me.DIAS_TRABAJTextBox = New System.Windows.Forms.TextBox()
        Me.DiasIncapTextBox = New System.Windows.Forms.TextBox()
        Me.InstitucionIncapacidadTextBox = New System.Windows.Forms.TextBox()
        Me.HORAS_EXTRAS_REGULARTextBox = New System.Windows.Forms.TextBox()
        Me.HORAS_EXTRAS_DOBLESTextBox = New System.Windows.Forms.TextBox()
        Me.TOTAL_DIAS_INCAPTextBox = New System.Windows.Forms.TextBox()
        Me.FechaINCAPINITextBox = New System.Windows.Forms.TextBox()
        Me.FechaINCAPFINTextBox = New System.Windows.Forms.TextBox()
        Me.AsientoTextBox = New System.Windows.Forms.TextBox()
        Me.IMP_RENTATextBox = New System.Windows.Forms.TextBox()
        Me.EXTRASTextBox = New System.Windows.Forms.TextBox()
        Me.CESANTIATextBox = New System.Windows.Forms.TextBox()
        Me.SUBSIDIOTextBox = New System.Windows.Forms.TextBox()
        Me.CodDedTextBox = New System.Windows.Forms.TextBox()
        Me.CCSSTextBox = New System.Windows.Forms.TextBox()
        Me.FechaPlanMensualDateTimePicker = New System.Windows.Forms.DateTimePicker()
        NoCEDULALabel = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        __ANUALLabel = New System.Windows.Forms.Label()
        ANUALIDADLabel = New System.Windows.Forms.Label()
        SALARIO_BRUTOLabel = New System.Windows.Forms.Label()
        SALARIO_DIARIOLabel = New System.Windows.Forms.Label()
        LIQUIDO_A_PAGARLabel = New System.Windows.Forms.Label()
        DIAS_TRABAJLabel = New System.Windows.Forms.Label()
        DiasIncapLabel = New System.Windows.Forms.Label()
        InstitucionIncapacidadLabel = New System.Windows.Forms.Label()
        HORAS_EXTRAS_REGULARLabel = New System.Windows.Forms.Label()
        HORAS_EXTRAS_DOBLESLabel = New System.Windows.Forms.Label()
        TOTAL_DIAS_INCAPLabel = New System.Windows.Forms.Label()
        FechaINCAPINILabel = New System.Windows.Forms.Label()
        FechaINCAPFINLabel = New System.Windows.Forms.Label()
        AsientoLabel = New System.Windows.Forms.Label()
        IMP_RENTALabel = New System.Windows.Forms.Label()
        EXTRASLabel = New System.Windows.Forms.Label()
        CESANTIALabel = New System.Windows.Forms.Label()
        SUBSIDIOLabel = New System.Windows.Forms.Label()
        CodDedLabel = New System.Windows.Forms.Label()
        CCSSLabel = New System.Windows.Forms.Label()
        FechaPlanMensualLabel1 = New System.Windows.Forms.Label()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PlanillaMensualBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PlanillaMensualBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PlanillaMensualBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'NoCEDULALabel
        '
        NoCEDULALabel.AutoSize = True
        NoCEDULALabel.Location = New System.Drawing.Point(149, 119)
        NoCEDULALabel.Name = "NoCEDULALabel"
        NoCEDULALabel.Size = New System.Drawing.Size(70, 13)
        NoCEDULALabel.TabIndex = 3
        NoCEDULALabel.Text = "No CEDULA:"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Location = New System.Drawing.Point(149, 145)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(47, 13)
        NombreLabel.TabIndex = 5
        NombreLabel.Text = "Nombre:"
        '
        '__ANUALLabel
        '
        __ANUALLabel.AutoSize = True
        __ANUALLabel.Location = New System.Drawing.Point(149, 171)
        __ANUALLabel.Name = "__ANUALLabel"
        __ANUALLabel.Size = New System.Drawing.Size(57, 13)
        __ANUALLabel.TabIndex = 7
        __ANUALLabel.Text = "% ANUAL:"
        '
        'ANUALIDADLabel
        '
        ANUALIDADLabel.AutoSize = True
        ANUALIDADLabel.Location = New System.Drawing.Point(149, 197)
        ANUALIDADLabel.Name = "ANUALIDADLabel"
        ANUALIDADLabel.Size = New System.Drawing.Size(72, 13)
        ANUALIDADLabel.TabIndex = 9
        ANUALIDADLabel.Text = "ANUALIDAD:"
        '
        'SALARIO_BRUTOLabel
        '
        SALARIO_BRUTOLabel.AutoSize = True
        SALARIO_BRUTOLabel.Location = New System.Drawing.Point(149, 223)
        SALARIO_BRUTOLabel.Name = "SALARIO_BRUTOLabel"
        SALARIO_BRUTOLabel.Size = New System.Drawing.Size(97, 13)
        SALARIO_BRUTOLabel.TabIndex = 11
        SALARIO_BRUTOLabel.Text = "SALARIO BRUTO:"
        '
        'SALARIO_DIARIOLabel
        '
        SALARIO_DIARIOLabel.AutoSize = True
        SALARIO_DIARIOLabel.Location = New System.Drawing.Point(149, 249)
        SALARIO_DIARIOLabel.Name = "SALARIO_DIARIOLabel"
        SALARIO_DIARIOLabel.Size = New System.Drawing.Size(96, 13)
        SALARIO_DIARIOLabel.TabIndex = 13
        SALARIO_DIARIOLabel.Text = "SALARIO DIARIO:"
        '
        'LIQUIDO_A_PAGARLabel
        '
        LIQUIDO_A_PAGARLabel.AutoSize = True
        LIQUIDO_A_PAGARLabel.Location = New System.Drawing.Point(149, 275)
        LIQUIDO_A_PAGARLabel.Name = "LIQUIDO_A_PAGARLabel"
        LIQUIDO_A_PAGARLabel.Size = New System.Drawing.Size(104, 13)
        LIQUIDO_A_PAGARLabel.TabIndex = 15
        LIQUIDO_A_PAGARLabel.Text = "LIQUIDO A PAGAR:"
        '
        'DIAS_TRABAJLabel
        '
        DIAS_TRABAJLabel.AutoSize = True
        DIAS_TRABAJLabel.Location = New System.Drawing.Point(149, 301)
        DIAS_TRABAJLabel.Name = "DIAS_TRABAJLabel"
        DIAS_TRABAJLabel.Size = New System.Drawing.Size(79, 13)
        DIAS_TRABAJLabel.TabIndex = 17
        DIAS_TRABAJLabel.Text = "DIAS TRABAJ:"
        '
        'DiasIncapLabel
        '
        DiasIncapLabel.AutoSize = True
        DiasIncapLabel.Location = New System.Drawing.Point(149, 327)
        DiasIncapLabel.Name = "DiasIncapLabel"
        DiasIncapLabel.Size = New System.Drawing.Size(61, 13)
        DiasIncapLabel.TabIndex = 19
        DiasIncapLabel.Text = "Dias Incap:"
        '
        'InstitucionIncapacidadLabel
        '
        InstitucionIncapacidadLabel.AutoSize = True
        InstitucionIncapacidadLabel.Location = New System.Drawing.Point(149, 353)
        InstitucionIncapacidadLabel.Name = "InstitucionIncapacidadLabel"
        InstitucionIncapacidadLabel.Size = New System.Drawing.Size(120, 13)
        InstitucionIncapacidadLabel.TabIndex = 21
        InstitucionIncapacidadLabel.Text = "Institucion Incapacidad:"
        '
        'HORAS_EXTRAS_REGULARLabel
        '
        HORAS_EXTRAS_REGULARLabel.AutoSize = True
        HORAS_EXTRAS_REGULARLabel.Location = New System.Drawing.Point(149, 379)
        HORAS_EXTRAS_REGULARLabel.Name = "HORAS_EXTRAS_REGULARLabel"
        HORAS_EXTRAS_REGULARLabel.Size = New System.Drawing.Size(149, 13)
        HORAS_EXTRAS_REGULARLabel.TabIndex = 23
        HORAS_EXTRAS_REGULARLabel.Text = "HORAS EXTRAS REGULAR:"
        '
        'HORAS_EXTRAS_DOBLESLabel
        '
        HORAS_EXTRAS_DOBLESLabel.AutoSize = True
        HORAS_EXTRAS_DOBLESLabel.Location = New System.Drawing.Point(149, 405)
        HORAS_EXTRAS_DOBLESLabel.Name = "HORAS_EXTRAS_DOBLESLabel"
        HORAS_EXTRAS_DOBLESLabel.Size = New System.Drawing.Size(140, 13)
        HORAS_EXTRAS_DOBLESLabel.TabIndex = 25
        HORAS_EXTRAS_DOBLESLabel.Text = "HORAS EXTRAS DOBLES:"
        '
        'TOTAL_DIAS_INCAPLabel
        '
        TOTAL_DIAS_INCAPLabel.AutoSize = True
        TOTAL_DIAS_INCAPLabel.Location = New System.Drawing.Point(149, 431)
        TOTAL_DIAS_INCAPLabel.Name = "TOTAL_DIAS_INCAPLabel"
        TOTAL_DIAS_INCAPLabel.Size = New System.Drawing.Size(108, 13)
        TOTAL_DIAS_INCAPLabel.TabIndex = 27
        TOTAL_DIAS_INCAPLabel.Text = "TOTAL DIAS INCAP:"
        '
        'FechaINCAPINILabel
        '
        FechaINCAPINILabel.AutoSize = True
        FechaINCAPINILabel.Location = New System.Drawing.Point(149, 456)
        FechaINCAPINILabel.Name = "FechaINCAPINILabel"
        FechaINCAPINILabel.Size = New System.Drawing.Size(89, 13)
        FechaINCAPINILabel.TabIndex = 29
        FechaINCAPINILabel.Text = "Fecha INCAPINI:"
        '
        'FechaINCAPFINLabel
        '
        FechaINCAPFINLabel.AutoSize = True
        FechaINCAPFINLabel.Location = New System.Drawing.Point(149, 482)
        FechaINCAPFINLabel.Name = "FechaINCAPFINLabel"
        FechaINCAPFINLabel.Size = New System.Drawing.Size(92, 13)
        FechaINCAPFINLabel.TabIndex = 31
        FechaINCAPFINLabel.Text = "Fecha INCAPFIN:"
        '
        'AsientoLabel
        '
        AsientoLabel.AutoSize = True
        AsientoLabel.Location = New System.Drawing.Point(149, 537)
        AsientoLabel.Name = "AsientoLabel"
        AsientoLabel.Size = New System.Drawing.Size(45, 13)
        AsientoLabel.TabIndex = 33
        AsientoLabel.Text = "Asiento:"
        '
        'IMP_RENTALabel
        '
        IMP_RENTALabel.AutoSize = True
        IMP_RENTALabel.Location = New System.Drawing.Point(149, 563)
        IMP_RENTALabel.Name = "IMP_RENTALabel"
        IMP_RENTALabel.Size = New System.Drawing.Size(69, 13)
        IMP_RENTALabel.TabIndex = 35
        IMP_RENTALabel.Text = "IMP RENTA:"
        '
        'EXTRASLabel
        '
        EXTRASLabel.AutoSize = True
        EXTRASLabel.Location = New System.Drawing.Point(149, 589)
        EXTRASLabel.Name = "EXTRASLabel"
        EXTRASLabel.Size = New System.Drawing.Size(53, 13)
        EXTRASLabel.TabIndex = 37
        EXTRASLabel.Text = "EXTRAS:"
        '
        'CESANTIALabel
        '
        CESANTIALabel.AutoSize = True
        CESANTIALabel.Location = New System.Drawing.Point(149, 615)
        CESANTIALabel.Name = "CESANTIALabel"
        CESANTIALabel.Size = New System.Drawing.Size(63, 13)
        CESANTIALabel.TabIndex = 39
        CESANTIALabel.Text = "CESANTIA:"
        '
        'SUBSIDIOLabel
        '
        SUBSIDIOLabel.AutoSize = True
        SUBSIDIOLabel.Location = New System.Drawing.Point(149, 641)
        SUBSIDIOLabel.Name = "SUBSIDIOLabel"
        SUBSIDIOLabel.Size = New System.Drawing.Size(61, 13)
        SUBSIDIOLabel.TabIndex = 41
        SUBSIDIOLabel.Text = "SUBSIDIO:"
        '
        'CodDedLabel
        '
        CodDedLabel.AutoSize = True
        CodDedLabel.Location = New System.Drawing.Point(149, 667)
        CodDedLabel.Name = "CodDedLabel"
        CodDedLabel.Size = New System.Drawing.Size(51, 13)
        CodDedLabel.TabIndex = 43
        CodDedLabel.Text = "cod Ded:"
        '
        'CCSSLabel
        '
        CCSSLabel.AutoSize = True
        CCSSLabel.Location = New System.Drawing.Point(149, 693)
        CCSSLabel.Name = "CCSSLabel"
        CCSSLabel.Size = New System.Drawing.Size(38, 13)
        CCSSLabel.TabIndex = 45
        CCSSLabel.Text = "CCSS:"
        '
        'FechaPlanMensualLabel1
        '
        FechaPlanMensualLabel1.AutoSize = True
        FechaPlanMensualLabel1.Location = New System.Drawing.Point(146, 90)
        FechaPlanMensualLabel1.Name = "FechaPlanMensualLabel1"
        FechaPlanMensualLabel1.Size = New System.Drawing.Size(107, 13)
        FechaPlanMensualLabel1.TabIndex = 46
        FechaPlanMensualLabel1.Text = "Fecha Plan Mensual:"
        '
        'Planilla2DataSet
        '
        Me.Planilla2DataSet.DataSetName = "Planilla2DataSet"
        Me.Planilla2DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PlanillaMensualBindingSource
        '
        Me.PlanillaMensualBindingSource.DataMember = "PlanillaMensual"
        Me.PlanillaMensualBindingSource.DataSource = Me.Planilla2DataSet
        '
        'PlanillaMensualTableAdapter
        '
        Me.PlanillaMensualTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AjustesTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DatosPersonalesTableAdapter = Nothing
        Me.TableAdapterManager.DeduccionesTableAdapter = Nothing
        Me.TableAdapterManager.ImpuestosTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaBisemanalTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaMensualTableAdapter = Me.PlanillaMensualTableAdapter
        Me.TableAdapterManager.PuestosTableAdapter = Nothing
        Me.TableAdapterManager.RegistroUsuarioTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'PlanillaMensualBindingNavigator
        '
        Me.PlanillaMensualBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.PlanillaMensualBindingNavigator.BindingSource = Me.PlanillaMensualBindingSource
        Me.PlanillaMensualBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.PlanillaMensualBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.PlanillaMensualBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.PlanillaMensualBindingNavigatorSaveItem})
        Me.PlanillaMensualBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.PlanillaMensualBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.PlanillaMensualBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.PlanillaMensualBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.PlanillaMensualBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.PlanillaMensualBindingNavigator.Name = "PlanillaMensualBindingNavigator"
        Me.PlanillaMensualBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.PlanillaMensualBindingNavigator.Size = New System.Drawing.Size(747, 25)
        Me.PlanillaMensualBindingNavigator.TabIndex = 0
        Me.PlanillaMensualBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem.Text = "Add new"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem.Text = "Delete"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'PlanillaMensualBindingNavigatorSaveItem
        '
        Me.PlanillaMensualBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.PlanillaMensualBindingNavigatorSaveItem.Image = CType(resources.GetObject("PlanillaMensualBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.PlanillaMensualBindingNavigatorSaveItem.Name = "PlanillaMensualBindingNavigatorSaveItem"
        Me.PlanillaMensualBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 22)
        Me.PlanillaMensualBindingNavigatorSaveItem.Text = "Save Data"
        '
        'NoCEDULATextBox
        '
        Me.NoCEDULATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "NoCEDULA", True))
        Me.NoCEDULATextBox.Location = New System.Drawing.Point(304, 116)
        Me.NoCEDULATextBox.Name = "NoCEDULATextBox"
        Me.NoCEDULATextBox.Size = New System.Drawing.Size(100, 20)
        Me.NoCEDULATextBox.TabIndex = 4
        '
        'NombreTextBox
        '
        Me.NombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "Nombre", True))
        Me.NombreTextBox.Location = New System.Drawing.Point(304, 142)
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.Size = New System.Drawing.Size(100, 20)
        Me.NombreTextBox.TabIndex = 6
        '
        '__ANUALTextBox
        '
        Me.__ANUALTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "% ANUAL", True))
        Me.__ANUALTextBox.Location = New System.Drawing.Point(304, 168)
        Me.__ANUALTextBox.Name = "__ANUALTextBox"
        Me.__ANUALTextBox.Size = New System.Drawing.Size(100, 20)
        Me.__ANUALTextBox.TabIndex = 8
        '
        'ANUALIDADTextBox
        '
        Me.ANUALIDADTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "ANUALIDAD", True))
        Me.ANUALIDADTextBox.Location = New System.Drawing.Point(304, 194)
        Me.ANUALIDADTextBox.Name = "ANUALIDADTextBox"
        Me.ANUALIDADTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ANUALIDADTextBox.TabIndex = 10
        '
        'SALARIO_BRUTOTextBox
        '
        Me.SALARIO_BRUTOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "SALARIO BRUTO", True))
        Me.SALARIO_BRUTOTextBox.Location = New System.Drawing.Point(304, 220)
        Me.SALARIO_BRUTOTextBox.Name = "SALARIO_BRUTOTextBox"
        Me.SALARIO_BRUTOTextBox.Size = New System.Drawing.Size(100, 20)
        Me.SALARIO_BRUTOTextBox.TabIndex = 12
        '
        'SALARIO_DIARIOTextBox
        '
        Me.SALARIO_DIARIOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "SALARIO DIARIO", True))
        Me.SALARIO_DIARIOTextBox.Location = New System.Drawing.Point(304, 246)
        Me.SALARIO_DIARIOTextBox.Name = "SALARIO_DIARIOTextBox"
        Me.SALARIO_DIARIOTextBox.Size = New System.Drawing.Size(100, 20)
        Me.SALARIO_DIARIOTextBox.TabIndex = 14
        '
        'LIQUIDO_A_PAGARTextBox
        '
        Me.LIQUIDO_A_PAGARTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "LIQUIDO A PAGAR", True))
        Me.LIQUIDO_A_PAGARTextBox.Location = New System.Drawing.Point(304, 272)
        Me.LIQUIDO_A_PAGARTextBox.Name = "LIQUIDO_A_PAGARTextBox"
        Me.LIQUIDO_A_PAGARTextBox.Size = New System.Drawing.Size(100, 20)
        Me.LIQUIDO_A_PAGARTextBox.TabIndex = 16
        '
        'DIAS_TRABAJTextBox
        '
        Me.DIAS_TRABAJTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "DIAS TRABAJ", True))
        Me.DIAS_TRABAJTextBox.Location = New System.Drawing.Point(304, 298)
        Me.DIAS_TRABAJTextBox.Name = "DIAS_TRABAJTextBox"
        Me.DIAS_TRABAJTextBox.Size = New System.Drawing.Size(100, 20)
        Me.DIAS_TRABAJTextBox.TabIndex = 18
        '
        'DiasIncapTextBox
        '
        Me.DiasIncapTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "DiasIncap", True))
        Me.DiasIncapTextBox.Location = New System.Drawing.Point(304, 324)
        Me.DiasIncapTextBox.Name = "DiasIncapTextBox"
        Me.DiasIncapTextBox.Size = New System.Drawing.Size(100, 20)
        Me.DiasIncapTextBox.TabIndex = 20
        '
        'InstitucionIncapacidadTextBox
        '
        Me.InstitucionIncapacidadTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "InstitucionIncapacidad", True))
        Me.InstitucionIncapacidadTextBox.Location = New System.Drawing.Point(304, 350)
        Me.InstitucionIncapacidadTextBox.Name = "InstitucionIncapacidadTextBox"
        Me.InstitucionIncapacidadTextBox.Size = New System.Drawing.Size(100, 20)
        Me.InstitucionIncapacidadTextBox.TabIndex = 22
        '
        'HORAS_EXTRAS_REGULARTextBox
        '
        Me.HORAS_EXTRAS_REGULARTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "HORAS EXTRAS REGULAR", True))
        Me.HORAS_EXTRAS_REGULARTextBox.Location = New System.Drawing.Point(304, 376)
        Me.HORAS_EXTRAS_REGULARTextBox.Name = "HORAS_EXTRAS_REGULARTextBox"
        Me.HORAS_EXTRAS_REGULARTextBox.Size = New System.Drawing.Size(100, 20)
        Me.HORAS_EXTRAS_REGULARTextBox.TabIndex = 24
        '
        'HORAS_EXTRAS_DOBLESTextBox
        '
        Me.HORAS_EXTRAS_DOBLESTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "HORAS EXTRAS DOBLES", True))
        Me.HORAS_EXTRAS_DOBLESTextBox.Location = New System.Drawing.Point(304, 402)
        Me.HORAS_EXTRAS_DOBLESTextBox.Name = "HORAS_EXTRAS_DOBLESTextBox"
        Me.HORAS_EXTRAS_DOBLESTextBox.Size = New System.Drawing.Size(100, 20)
        Me.HORAS_EXTRAS_DOBLESTextBox.TabIndex = 26
        '
        'TOTAL_DIAS_INCAPTextBox
        '
        Me.TOTAL_DIAS_INCAPTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "TOTAL DIAS INCAP", True))
        Me.TOTAL_DIAS_INCAPTextBox.Location = New System.Drawing.Point(304, 428)
        Me.TOTAL_DIAS_INCAPTextBox.Name = "TOTAL_DIAS_INCAPTextBox"
        Me.TOTAL_DIAS_INCAPTextBox.Size = New System.Drawing.Size(100, 20)
        Me.TOTAL_DIAS_INCAPTextBox.TabIndex = 28
        '
        'FechaINCAPINITextBox
        '
        Me.FechaINCAPINITextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "FechaINCAPINI", True))
        Me.FechaINCAPINITextBox.Location = New System.Drawing.Point(304, 453)
        Me.FechaINCAPINITextBox.Name = "FechaINCAPINITextBox"
        Me.FechaINCAPINITextBox.Size = New System.Drawing.Size(100, 20)
        Me.FechaINCAPINITextBox.TabIndex = 30
        '
        'FechaINCAPFINTextBox
        '
        Me.FechaINCAPFINTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "FechaINCAPFIN", True))
        Me.FechaINCAPFINTextBox.Location = New System.Drawing.Point(304, 479)
        Me.FechaINCAPFINTextBox.Name = "FechaINCAPFINTextBox"
        Me.FechaINCAPFINTextBox.Size = New System.Drawing.Size(100, 20)
        Me.FechaINCAPFINTextBox.TabIndex = 32
        '
        'AsientoTextBox
        '
        Me.AsientoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "Asiento", True))
        Me.AsientoTextBox.Location = New System.Drawing.Point(304, 534)
        Me.AsientoTextBox.Name = "AsientoTextBox"
        Me.AsientoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.AsientoTextBox.TabIndex = 34
        '
        'IMP_RENTATextBox
        '
        Me.IMP_RENTATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "IMP RENTA", True))
        Me.IMP_RENTATextBox.Location = New System.Drawing.Point(304, 560)
        Me.IMP_RENTATextBox.Name = "IMP_RENTATextBox"
        Me.IMP_RENTATextBox.Size = New System.Drawing.Size(100, 20)
        Me.IMP_RENTATextBox.TabIndex = 36
        '
        'EXTRASTextBox
        '
        Me.EXTRASTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "EXTRAS", True))
        Me.EXTRASTextBox.Location = New System.Drawing.Point(304, 586)
        Me.EXTRASTextBox.Name = "EXTRASTextBox"
        Me.EXTRASTextBox.Size = New System.Drawing.Size(100, 20)
        Me.EXTRASTextBox.TabIndex = 38
        '
        'CESANTIATextBox
        '
        Me.CESANTIATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "CESANTIA", True))
        Me.CESANTIATextBox.Location = New System.Drawing.Point(304, 612)
        Me.CESANTIATextBox.Name = "CESANTIATextBox"
        Me.CESANTIATextBox.Size = New System.Drawing.Size(100, 20)
        Me.CESANTIATextBox.TabIndex = 40
        '
        'SUBSIDIOTextBox
        '
        Me.SUBSIDIOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "SUBSIDIO", True))
        Me.SUBSIDIOTextBox.Location = New System.Drawing.Point(304, 638)
        Me.SUBSIDIOTextBox.Name = "SUBSIDIOTextBox"
        Me.SUBSIDIOTextBox.Size = New System.Drawing.Size(100, 20)
        Me.SUBSIDIOTextBox.TabIndex = 42
        '
        'CodDedTextBox
        '
        Me.CodDedTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "codDed", True))
        Me.CodDedTextBox.Location = New System.Drawing.Point(304, 664)
        Me.CodDedTextBox.Name = "CodDedTextBox"
        Me.CodDedTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CodDedTextBox.TabIndex = 44
        '
        'CCSSTextBox
        '
        Me.CCSSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PlanillaMensualBindingSource, "CCSS", True))
        Me.CCSSTextBox.Location = New System.Drawing.Point(304, 690)
        Me.CCSSTextBox.Name = "CCSSTextBox"
        Me.CCSSTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CCSSTextBox.TabIndex = 46
        '
        'FechaPlanMensualDateTimePicker
        '
        Me.FechaPlanMensualDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.PlanillaMensualBindingSource, "FechaPlanMensual", True))
        Me.FechaPlanMensualDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FechaPlanMensualDateTimePicker.Location = New System.Drawing.Point(304, 84)
        Me.FechaPlanMensualDateTimePicker.Name = "FechaPlanMensualDateTimePicker"
        Me.FechaPlanMensualDateTimePicker.Size = New System.Drawing.Size(100, 20)
        Me.FechaPlanMensualDateTimePicker.TabIndex = 47
        '
        'frmPlanillaMensual
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(747, 731)
        Me.Controls.Add(FechaPlanMensualLabel1)
        Me.Controls.Add(Me.FechaPlanMensualDateTimePicker)
        Me.Controls.Add(NoCEDULALabel)
        Me.Controls.Add(Me.NoCEDULATextBox)
        Me.Controls.Add(NombreLabel)
        Me.Controls.Add(Me.NombreTextBox)
        Me.Controls.Add(__ANUALLabel)
        Me.Controls.Add(Me.__ANUALTextBox)
        Me.Controls.Add(ANUALIDADLabel)
        Me.Controls.Add(Me.ANUALIDADTextBox)
        Me.Controls.Add(SALARIO_BRUTOLabel)
        Me.Controls.Add(Me.SALARIO_BRUTOTextBox)
        Me.Controls.Add(SALARIO_DIARIOLabel)
        Me.Controls.Add(Me.SALARIO_DIARIOTextBox)
        Me.Controls.Add(LIQUIDO_A_PAGARLabel)
        Me.Controls.Add(Me.LIQUIDO_A_PAGARTextBox)
        Me.Controls.Add(DIAS_TRABAJLabel)
        Me.Controls.Add(Me.DIAS_TRABAJTextBox)
        Me.Controls.Add(DiasIncapLabel)
        Me.Controls.Add(Me.DiasIncapTextBox)
        Me.Controls.Add(InstitucionIncapacidadLabel)
        Me.Controls.Add(Me.InstitucionIncapacidadTextBox)
        Me.Controls.Add(HORAS_EXTRAS_REGULARLabel)
        Me.Controls.Add(Me.HORAS_EXTRAS_REGULARTextBox)
        Me.Controls.Add(HORAS_EXTRAS_DOBLESLabel)
        Me.Controls.Add(Me.HORAS_EXTRAS_DOBLESTextBox)
        Me.Controls.Add(TOTAL_DIAS_INCAPLabel)
        Me.Controls.Add(Me.TOTAL_DIAS_INCAPTextBox)
        Me.Controls.Add(FechaINCAPINILabel)
        Me.Controls.Add(Me.FechaINCAPINITextBox)
        Me.Controls.Add(FechaINCAPFINLabel)
        Me.Controls.Add(Me.FechaINCAPFINTextBox)
        Me.Controls.Add(AsientoLabel)
        Me.Controls.Add(Me.AsientoTextBox)
        Me.Controls.Add(IMP_RENTALabel)
        Me.Controls.Add(Me.IMP_RENTATextBox)
        Me.Controls.Add(EXTRASLabel)
        Me.Controls.Add(Me.EXTRASTextBox)
        Me.Controls.Add(CESANTIALabel)
        Me.Controls.Add(Me.CESANTIATextBox)
        Me.Controls.Add(SUBSIDIOLabel)
        Me.Controls.Add(Me.SUBSIDIOTextBox)
        Me.Controls.Add(CodDedLabel)
        Me.Controls.Add(Me.CodDedTextBox)
        Me.Controls.Add(CCSSLabel)
        Me.Controls.Add(Me.CCSSTextBox)
        Me.Controls.Add(Me.PlanillaMensualBindingNavigator)
        Me.Name = "frmPlanillaMensual"
        Me.Text = "frmPlanillaMensual"
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PlanillaMensualBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PlanillaMensualBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PlanillaMensualBindingNavigator.ResumeLayout(False)
        Me.PlanillaMensualBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Planilla2DataSet As PlanillasProyecto3.Planilla2DataSet
    Friend WithEvents PlanillaMensualBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PlanillaMensualTableAdapter As PlanillasProyecto3.Planilla2DataSetTableAdapters.PlanillaMensualTableAdapter
    Friend WithEvents TableAdapterManager As PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager
    Friend WithEvents PlanillaMensualBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PlanillaMensualBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents NoCEDULATextBox As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents __ANUALTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ANUALIDADTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SALARIO_BRUTOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SALARIO_DIARIOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents LIQUIDO_A_PAGARTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DIAS_TRABAJTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DiasIncapTextBox As System.Windows.Forms.TextBox
    Friend WithEvents InstitucionIncapacidadTextBox As System.Windows.Forms.TextBox
    Friend WithEvents HORAS_EXTRAS_REGULARTextBox As System.Windows.Forms.TextBox
    Friend WithEvents HORAS_EXTRAS_DOBLESTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TOTAL_DIAS_INCAPTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FechaINCAPINITextBox As System.Windows.Forms.TextBox
    Friend WithEvents FechaINCAPFINTextBox As System.Windows.Forms.TextBox
    Friend WithEvents AsientoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents IMP_RENTATextBox As System.Windows.Forms.TextBox
    Friend WithEvents EXTRASTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CESANTIATextBox As System.Windows.Forms.TextBox
    Friend WithEvents SUBSIDIOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CodDedTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CCSSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FechaPlanMensualDateTimePicker As System.Windows.Forms.DateTimePicker
End Class
