﻿Public Class frmDeduccionesConsulta

    Private Sub DeduccionesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.DeduccionesBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub frmDeduccionesConsulta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Planilla2DataSet.Deducciones' table. You can move, or remove it, as needed.
        Me.DeduccionesTableAdapter.Fill(Me.Planilla2DataSet.Deducciones)
        txtDatoConsulta.Focus()

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Try

            If Me.DeduccionesTableAdapter.FillBy(Me.Planilla2DataSet.Deducciones, Integer.Parse(txtDatoConsulta.Text)) = True Then
            Else
                MsgBox("Registro no encontrado")
            End If

        Catch ex As Exception
            MsgBox("Verifique los parametros de busqueda: " + ex.Message)
        End Try
    End Sub

    Private Sub btnMostrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMostrar.Click
        Me.DeduccionesTableAdapter.Fill(Me.Planilla2DataSet.Deducciones)
    End Sub
End Class