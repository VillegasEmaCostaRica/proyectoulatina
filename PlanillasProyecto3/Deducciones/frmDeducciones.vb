﻿Public Class frmDeducciones
    Private intOpcion As Integer
    Private Sub RegresarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegresarToolStripMenuItem.Click
        frmPrincipal.Show()
        Me.Close()
    End Sub

    Private Sub ConsultarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarToolStripMenuItem.Click
        frmDeduccionesConsulta.Show()
    End Sub

   
    Private Sub frmDeducciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Planilla2DataSet.Deducciones' table. You can move, or remove it, as needed.
        Me.DeduccionesTableAdapter.Fill(Me.Planilla2DataSet.Deducciones)
        limmpiarCajasTexto()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizar.Click
        Try
            Me.Validate()
            Me.DeduccionesBindingSource.EndEdit()
            Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)
            limmpiarCajasTexto()
            MsgBox("Actualización exitosa")
        Catch ex As Exception
            MsgBox("Error" + ex.Message)
        End Try
       
    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        Try
            Me.DeduccionesTableAdapter.InsertQuery(Integer.Parse(CodDeduccionTextBox.Text), Integer.Parse(MENSUALTextBox.Text), Integer.Parse(CATORCENALTextBox.Text), Integer.Parse(PERSONALTextBox.Text), Integer.Parse(PATRONALTextBox.Text), Integer.Parse(REBAJOEMBARGOTextBox.Text), Integer.Parse(DEDUCCCOLEGIOTextBox.Text), Integer.Parse(Fiesta_JefaturasTextBox.Text), Integer.Parse(Fiesta_Fin_de_AñoTextBox.Text), Integer.Parse(OTRAS_DEDUCCTextBox.Text), Integer.Parse(FondoMutualTextBox.Text), Integer.Parse(Préstamos_BcoPopularTextBox.Text), Integer.Parse(SumaTotalTextBox.Text))
            limmpiarCajasTexto()
            MsgBox("Datos almacenados exitosamente")
        Catch frm As FormatException
            MsgBox("Sólo se aceptan números")
        Catch ex As Exception
            MsgBox("Verifique los datos de ingreso por favor")
        End Try
      
    End Sub

    Private Sub SumaTotalTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SumaTotalTextBox.GotFocus
        SumaTotalTextBox.Text = Integer.Parse(CodDeduccionTextBox.Text) + Integer.Parse(MENSUALTextBox.Text) + Integer.Parse(CATORCENALTextBox.Text) + Integer.Parse(PERSONALTextBox.Text) + Integer.Parse(PATRONALTextBox.Text) + Integer.Parse(REBAJOEMBARGOTextBox.Text) + Integer.Parse(DEDUCCCOLEGIOTextBox.Text) + Integer.Parse(Fiesta_JefaturasTextBox.Text) + Integer.Parse(Fiesta_Fin_de_AñoTextBox.Text) + Integer.Parse(OTRAS_DEDUCCTextBox.Text) + Integer.Parse(FondoMutualTextBox.Text) + Integer.Parse(Préstamos_BcoPopularTextBox.Text)
    End Sub


    Sub limmpiarCajasTexto()


        CodDeduccionTextBox.Text = 0
        CodDeduccionTextBox.Focus()
        MENSUALTextBox.Clear()
        CATORCENALTextBox.Clear()
        PERSONALTextBox.Clear()
        PATRONALTextBox.Clear()
        REBAJOEMBARGOTextBox.Clear()
        DEDUCCCOLEGIOTextBox.Clear()
        Fiesta_JefaturasTextBox.Clear()
        Fiesta_Fin_de_AñoTextBox.Clear()
        OTRAS_DEDUCCTextBox.Clear()
        FondoMutualTextBox.Clear()
        Préstamos_BcoPopularTextBox.Clear()
        SumaTotalTextBox.Clear()
    End Sub

    Private Sub CodDeduccionTextBox_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles REBAJOEMBARGOTextBox.Click, Préstamos_BcoPopularTextBox.Click, PERSONALTextBox.Click, PATRONALTextBox.Click, OTRAS_DEDUCCTextBox.Click, MENSUALTextBox.Click, FondoMutualTextBox.Click, Fiesta_JefaturasTextBox.Click, Fiesta_Fin_de_AñoTextBox.Click, DEDUCCCOLEGIOTextBox.Click, CodDeduccionTextBox.Click, CATORCENALTextBox.Click
        Dim objTexi As TextBox
        objTexi = CType(sender, TextBox)
        objTexi.SelectAll()
    End Sub


    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Try


            intOpcion = Integer.Parse(InputBox("Ingrese el año año a buscar"))

            If (Me.DeduccionesTableAdapter.FillBy(Me.Planilla2DataSet.Deducciones, Integer.Parse(intOpcion)) = True) Then
                StatusStrip1.Text = "Busqueda exitosa"

            Else
                MsgBox("Registro no encontrado")
            End If
        Catch ex As Exception
            MsgBox("Verifique los datos de entrada")
        Finally
            StatusStrip1.Text = "Status"
        End Try


    End Sub

    Private Sub btnBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBorrar.Click


        Try
            If MsgBox("Desea Borrar el registro", vbYesNo) = vbYes Then

                Me.DeduccionesTableAdapter.DeleteQuery(Integer.Parse(CodDeduccionTextBox.Text))
                MsgBox("Registro borrado exitosamente")
                limmpiarCajasTexto()
            Else

            End If
        Catch ex As Exception
            MsgBox("Error" + ex.Message)
        End Try
    End Sub
End Class