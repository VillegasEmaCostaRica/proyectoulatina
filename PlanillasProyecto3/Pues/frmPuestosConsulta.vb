﻿Public Class frmPuestosConsulta

    Private Sub PuestosBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.PuestosBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub frmPuestosConsulta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Planilla2DataSet.Puestos' table. You can move, or remove it, as needed.
        Me.PuestosTableAdapter.Fill(Me.Planilla2DataSet.Puestos)
        txtDatoConsulta.Focus()

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Try
            departamento = txtDatoConsulta.Text


            If Me.PuestosTableAdapter.FillBy2buscarPuesto(Me.Planilla2DataSet.Puestos, departamento) = "0" Then
                ''Me.PuestosTableAdapter.FillBy2buscarPuesto(Me.Planilla2DataSet.Puestos, departamento)
                ''Else
                MsgBox("No se Encontro Ningun Registro con este Nombre de Departamento")
                txtDatoConsulta.Clear()
                txtDatoConsulta.Focus()
                Me.PuestosTableAdapter.Fill(Me.Planilla2DataSet.Puestos)

            End If



        Catch ex As Exception
            MsgBox("No se Encontro Ningun Registro con este Nombre de Departamento")
            txtDatoConsulta.Clear()
            txtDatoConsulta.Focus()
            Me.PuestosTableAdapter.Fill(Me.Planilla2DataSet.Puestos)

        End Try
    End Sub

    
    Private Sub PuestosDataGridView_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PuestosDataGridView.DoubleClick

        codPuesto = Integer.Parse(PuestosDataGridView.Item(0, PuestosDataGridView.CurrentRow.Index).Value)


        Select Case tipoFormularioPuestoValidar
            Case "Actualizar"
                frmPuestosActualizar.PuestosTableAdapter.FillByBuscarPorCodigo(frmPuestosActualizar.Planilla2DataSet.Puestos, codPuesto)
                Me.Close()
                frmPuestosActualizar.Show()
            Case "Eliminar"

                frmPuestoEliminar.PuestosTableAdapter.FillByBuscarPorCodigo(frmPuestoEliminar.Planilla2DataSet.Puestos, codPuesto)
                frmPuestoEliminar.txtDatoConsulta.Text = codPuesto
                Me.Close()
                frmPuestoEliminar.Show()
            Case Else

        End Select






    End Sub


End Class