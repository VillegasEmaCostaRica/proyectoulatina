﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPuestos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CODIGO_DE_PUESTOLabel As System.Windows.Forms.Label
        Dim DESCRIPCIONLabel As System.Windows.Forms.Label
        Dim DEPARTAMENTOLabel As System.Windows.Forms.Label
        Dim SALARIO_UNICOLabel As System.Windows.Forms.Label
        Dim EMPLEADO_CONFIANZALabel As System.Windows.Forms.Label
        Dim GRADO_ACADEMICOLabel As System.Windows.Forms.Label
        Me.Planilla2DataSet = New PlanillasProyecto3.Planilla2DataSet()
        Me.PuestosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PuestosTableAdapter = New PlanillasProyecto3.Planilla2DataSetTableAdapters.PuestosTableAdapter()
        Me.TableAdapterManager = New PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager()
        Me.CODIGO_DE_PUESTOTextBox = New System.Windows.Forms.TextBox()
        Me.DESCRIPCIONTextBox = New System.Windows.Forms.TextBox()
        Me.DEPARTAMENTOTextBox = New System.Windows.Forms.TextBox()
        Me.SALARIO_UNICOTextBox = New System.Windows.Forms.TextBox()
        Me.EMPLEADO_CONFIANZACheckBox = New System.Windows.Forms.CheckBox()
        Me.GRADO_ACADEMICOTextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnGuardar = New System.Windows.Forms.Button()
        CODIGO_DE_PUESTOLabel = New System.Windows.Forms.Label()
        DESCRIPCIONLabel = New System.Windows.Forms.Label()
        DEPARTAMENTOLabel = New System.Windows.Forms.Label()
        SALARIO_UNICOLabel = New System.Windows.Forms.Label()
        EMPLEADO_CONFIANZALabel = New System.Windows.Forms.Label()
        GRADO_ACADEMICOLabel = New System.Windows.Forms.Label()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PuestosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CODIGO_DE_PUESTOLabel
        '
        CODIGO_DE_PUESTOLabel.AutoSize = True
        CODIGO_DE_PUESTOLabel.Location = New System.Drawing.Point(22, 40)
        CODIGO_DE_PUESTOLabel.Name = "CODIGO_DE_PUESTOLabel"
        CODIGO_DE_PUESTOLabel.Size = New System.Drawing.Size(117, 13)
        CODIGO_DE_PUESTOLabel.TabIndex = 1
        CODIGO_DE_PUESTOLabel.Text = "CODIGO DE PUESTO:"
        '
        'DESCRIPCIONLabel
        '
        DESCRIPCIONLabel.AutoSize = True
        DESCRIPCIONLabel.Location = New System.Drawing.Point(22, 66)
        DESCRIPCIONLabel.Name = "DESCRIPCIONLabel"
        DESCRIPCIONLabel.Size = New System.Drawing.Size(83, 13)
        DESCRIPCIONLabel.TabIndex = 3
        DESCRIPCIONLabel.Text = "DESCRIPCION:"
        '
        'DEPARTAMENTOLabel
        '
        DEPARTAMENTOLabel.AutoSize = True
        DEPARTAMENTOLabel.Location = New System.Drawing.Point(22, 92)
        DEPARTAMENTOLabel.Name = "DEPARTAMENTOLabel"
        DEPARTAMENTOLabel.Size = New System.Drawing.Size(100, 13)
        DEPARTAMENTOLabel.TabIndex = 5
        DEPARTAMENTOLabel.Text = "DEPARTAMENTO:"
        '
        'SALARIO_UNICOLabel
        '
        SALARIO_UNICOLabel.AutoSize = True
        SALARIO_UNICOLabel.Location = New System.Drawing.Point(22, 118)
        SALARIO_UNICOLabel.Name = "SALARIO_UNICOLabel"
        SALARIO_UNICOLabel.Size = New System.Drawing.Size(93, 13)
        SALARIO_UNICOLabel.TabIndex = 7
        SALARIO_UNICOLabel.Text = "SALARIO UNICO:"
        '
        'EMPLEADO_CONFIANZALabel
        '
        EMPLEADO_CONFIANZALabel.AutoSize = True
        EMPLEADO_CONFIANZALabel.Location = New System.Drawing.Point(22, 146)
        EMPLEADO_CONFIANZALabel.Name = "EMPLEADO_CONFIANZALabel"
        EMPLEADO_CONFIANZALabel.Size = New System.Drawing.Size(133, 13)
        EMPLEADO_CONFIANZALabel.TabIndex = 9
        EMPLEADO_CONFIANZALabel.Text = "EMPLEADO CONFIANZA:"
        '
        'GRADO_ACADEMICOLabel
        '
        GRADO_ACADEMICOLabel.AutoSize = True
        GRADO_ACADEMICOLabel.Location = New System.Drawing.Point(22, 174)
        GRADO_ACADEMICOLabel.Name = "GRADO_ACADEMICOLabel"
        GRADO_ACADEMICOLabel.Size = New System.Drawing.Size(115, 13)
        GRADO_ACADEMICOLabel.TabIndex = 11
        GRADO_ACADEMICOLabel.Text = "GRADO ACADEMICO:"
        '
        'Planilla2DataSet
        '
        Me.Planilla2DataSet.DataSetName = "Planilla2DataSet"
        Me.Planilla2DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PuestosBindingSource
        '
        Me.PuestosBindingSource.DataMember = "Puestos"
        Me.PuestosBindingSource.DataSource = Me.Planilla2DataSet
        '
        'PuestosTableAdapter
        '
        Me.PuestosTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AjustesTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DatosPersonalesTableAdapter = Nothing
        Me.TableAdapterManager.DeduccionesTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaBisemanalTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaMensualTableAdapter = Nothing
        Me.TableAdapterManager.PuestosTableAdapter = Me.PuestosTableAdapter
        Me.TableAdapterManager.UpdateOrder = PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'CODIGO_DE_PUESTOTextBox
        '
        Me.CODIGO_DE_PUESTOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "CODIGO DE PUESTO", True))
        Me.CODIGO_DE_PUESTOTextBox.Location = New System.Drawing.Point(161, 37)
        Me.CODIGO_DE_PUESTOTextBox.Name = "CODIGO_DE_PUESTOTextBox"
        Me.CODIGO_DE_PUESTOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.CODIGO_DE_PUESTOTextBox.TabIndex = 2
        '
        'DESCRIPCIONTextBox
        '
        Me.DESCRIPCIONTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "DESCRIPCION", True))
        Me.DESCRIPCIONTextBox.Location = New System.Drawing.Point(161, 63)
        Me.DESCRIPCIONTextBox.Name = "DESCRIPCIONTextBox"
        Me.DESCRIPCIONTextBox.Size = New System.Drawing.Size(104, 20)
        Me.DESCRIPCIONTextBox.TabIndex = 4
        '
        'DEPARTAMENTOTextBox
        '
        Me.DEPARTAMENTOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "DEPARTAMENTO", True))
        Me.DEPARTAMENTOTextBox.Location = New System.Drawing.Point(161, 89)
        Me.DEPARTAMENTOTextBox.Name = "DEPARTAMENTOTextBox"
        Me.DEPARTAMENTOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.DEPARTAMENTOTextBox.TabIndex = 6
        '
        'SALARIO_UNICOTextBox
        '
        Me.SALARIO_UNICOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "SALARIO UNICO", True))
        Me.SALARIO_UNICOTextBox.Location = New System.Drawing.Point(161, 115)
        Me.SALARIO_UNICOTextBox.Name = "SALARIO_UNICOTextBox"
        Me.SALARIO_UNICOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.SALARIO_UNICOTextBox.TabIndex = 8
        '
        'EMPLEADO_CONFIANZACheckBox
        '
        Me.EMPLEADO_CONFIANZACheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.PuestosBindingSource, "EMPLEADO CONFIANZA", True))
        Me.EMPLEADO_CONFIANZACheckBox.Location = New System.Drawing.Point(161, 141)
        Me.EMPLEADO_CONFIANZACheckBox.Name = "EMPLEADO_CONFIANZACheckBox"
        Me.EMPLEADO_CONFIANZACheckBox.Size = New System.Drawing.Size(104, 24)
        Me.EMPLEADO_CONFIANZACheckBox.TabIndex = 10
        Me.EMPLEADO_CONFIANZACheckBox.Text = "Si"
        Me.EMPLEADO_CONFIANZACheckBox.UseVisualStyleBackColor = True
        '
        'GRADO_ACADEMICOTextBox
        '
        Me.GRADO_ACADEMICOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PuestosBindingSource, "GRADO ACADEMICO", True))
        Me.GRADO_ACADEMICOTextBox.Location = New System.Drawing.Point(161, 171)
        Me.GRADO_ACADEMICOTextBox.Name = "GRADO_ACADEMICOTextBox"
        Me.GRADO_ACADEMICOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.GRADO_ACADEMICOTextBox.TabIndex = 12
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnGuardar)
        Me.GroupBox1.Controls.Add(CODIGO_DE_PUESTOLabel)
        Me.GroupBox1.Controls.Add(Me.GRADO_ACADEMICOTextBox)
        Me.GroupBox1.Controls.Add(Me.CODIGO_DE_PUESTOTextBox)
        Me.GroupBox1.Controls.Add(GRADO_ACADEMICOLabel)
        Me.GroupBox1.Controls.Add(DESCRIPCIONLabel)
        Me.GroupBox1.Controls.Add(Me.EMPLEADO_CONFIANZACheckBox)
        Me.GroupBox1.Controls.Add(Me.DESCRIPCIONTextBox)
        Me.GroupBox1.Controls.Add(EMPLEADO_CONFIANZALabel)
        Me.GroupBox1.Controls.Add(DEPARTAMENTOLabel)
        Me.GroupBox1.Controls.Add(Me.SALARIO_UNICOTextBox)
        Me.GroupBox1.Controls.Add(Me.DEPARTAMENTOTextBox)
        Me.GroupBox1.Controls.Add(SALARIO_UNICOLabel)
        Me.GroupBox1.Location = New System.Drawing.Point(88, 77)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(383, 268)
        Me.GroupBox1.TabIndex = 13
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Guardar Puesto"
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(139, 223)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 13
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'frmPuestos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(728, 369)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmPuestos"
        Me.Text = "frmPuestos"
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PuestosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Planilla2DataSet As PlanillasProyecto3.Planilla2DataSet
    Friend WithEvents PuestosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PuestosTableAdapter As PlanillasProyecto3.Planilla2DataSetTableAdapters.PuestosTableAdapter
    Friend WithEvents TableAdapterManager As PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager
    Friend WithEvents CODIGO_DE_PUESTOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DESCRIPCIONTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DEPARTAMENTOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SALARIO_UNICOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EMPLEADO_CONFIANZACheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents GRADO_ACADEMICOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
End Class
