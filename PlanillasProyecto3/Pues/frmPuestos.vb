﻿Public Class frmPuestos

    Private Sub PuestosBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.PuestosBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub


    Private Sub PuestosBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.PuestosBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub frmPuestos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet.Puestos' Puede moverla o quitarla según sea necesario.
        'Me.PuestosTableAdapter.Fill(Me.Planilla2DataSet.Puestos)

    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Try

            If EMPLEADO_CONFIANZACheckBox.Checked = True Then

                empleadoConfianza = True
            Else
                empleadoConfianza = False

            End If

            Me.PuestosTableAdapter.InsertPuesto(Integer.Parse(CODIGO_DE_PUESTOTextBox.Text), DESCRIPCIONTextBox.Text, DEPARTAMENTOTextBox.Text, Integer.Parse(SALARIO_UNICOTextBox.Text), empleadoConfianza, GRADO_ACADEMICOTextBox.Text)
            Me.PuestosTableAdapter.Fill(Me.Planilla2DataSet.Puestos)
        Catch ex As Exception
            MsgBox("No Se Guardo los Datos porque Coincide con un Codigo ya Guardado ")
        End Try
    End Sub

    Private Sub EMPLEADO_CONFIANZACheckBox_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EMPLEADO_CONFIANZACheckBox.Enter
        ' EMPLEADO_CONFIANZACheckBox.Enter = True
    End Sub
End Class