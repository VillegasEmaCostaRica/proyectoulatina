﻿Public Class frmPrincipal

    Private Sub IngresarDatosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IngresarDatosToolStripMenuItem.Click
        frmDatosEmpleado.Show()
    End Sub

    Private Sub ConsultarDatosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarDatosToolStripMenuItem.Click
        frmDatosEmpleadoConsulta.Show()
    End Sub

    Private Sub IngresarPuestosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IngresarPuestosToolStripMenuItem.Click
        frmPuestos.Show()
    End Sub

    Private Sub ConsultarPuestosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarPuestosToolStripMenuItem.Click
        frmPuestosConsulta.Show()
    End Sub

    Private Sub IngresarAjustesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IngresarAjustesToolStripMenuItem.Click
        frmAjustes.Show()
           End Sub

    Private Sub ConsultarAjustesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarAjustesToolStripMenuItem.Click
        frmConsulataAjustes.Show()

    End Sub

    Private Sub IngresarDeduccionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IngresarDeduccionesToolStripMenuItem.Click
        frmDeducciones.Show()
    End Sub

    Private Sub ConsultarDedueccionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarDedueccionesToolStripMenuItem.Click
        frmDeduccionesConsulta.Show()

    End Sub

    Private Sub IngresarPlanillaMensualToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IngresarPlanillaMensualToolStripMenuItem.Click
        frmPlanillaMensual.Show()
    End Sub

    Private Sub ConsultarPlanillaMensualToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarPlanillaMensualToolStripMenuItem.Click
        frmPlanillaMensualConsulta.Show()
    End Sub

    Private Sub IngresarPlanillaBisemanalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IngresarPlanillaBisemanalToolStripMenuItem.Click
        frmPlanillaBisemanal.Show()
    End Sub
    Private Sub ConsultarPlanillaBisemanalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarPlanillaBisemanalToolStripMenuItem.Click
        frmPlanillaBisemanalConsulta.Show()
    End Sub

    Private Sub DeduccucionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeduccucionesToolStripMenuItem.Click

    End Sub

    Private Sub frmPrincipal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Planilla2DataSet.Ajustes' table. You can move, or remove it, as needed.
        Me.AjustesTableAdapter.Fill(Me.Planilla2DataSet.Ajustes)

    End Sub

    Private Sub ActualizarDatosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActualizarDatosToolStripMenuItem.Click

        formdatosEmpleadosActualizar.Show()
    End Sub

    Private Sub EliminarDatosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarDatosToolStripMenuItem.Click
        frmDatosEmpleadoEliminar.Show()
    End Sub

    Private Sub ActualizarPuestoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActualizarPuestoToolStripMenuItem.Click
        frmPuestosActualizar.Show()
    End Sub

    Private Sub EliminarPuestoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarPuestoToolStripMenuItem.Click
        frmPuestoEliminar.Show()
    End Sub

    Private Sub AgregarUsuarioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AgregarUsuarioToolStripMenuItem.Click
        OperacionUsuario("Ingresar")
    End Sub

    Private Sub EliminarUsuarioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarUsuarioToolStripMenuItem.Click
        OperacionUsuario("Eliminar")
    End Sub

    Private Sub ActualizarUsuarioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActualizarUsuarioToolStripMenuItem.Click
        OperacionUsuario("Actualizar")
    End Sub

    Private Sub IngresarDatosToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IngresarDatosToolStripMenuItem1.Click
        frmImpuesto.Show()

    End Sub

    Private Sub ConsultarRegistrosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarRegistrosToolStripMenuItem.Click
        frmConsultarImpuesto.Show()

    End Sub

    Private Sub AjustesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ImpuestosFiscalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImpuestosFiscalesToolStripMenuItem.Click

    End Sub
End Class