﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImpuesto
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CodImpuestoLabel As System.Windows.Forms.Label
        Dim ProcentajeImpuesto1Label As System.Windows.Forms.Label
        Dim ProcentajeImpuesto2Label As System.Windows.Forms.Label
        Dim CreditosFiscalesHijoLabel As System.Windows.Forms.Label
        Dim CreditosFiscalesConyugeLabel As System.Windows.Forms.Label
        Me.Planilla2DataSet = New PlanillasProyecto3.Planilla2DataSet()
        Me.ImpuestosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ImpuestosTableAdapter = New PlanillasProyecto3.Planilla2DataSetTableAdapters.ImpuestosTableAdapter()
        Me.TableAdapterManager = New PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager()
        Me.CodImpuestoTextBox = New System.Windows.Forms.TextBox()
        Me.ProcentajeImpuesto1TextBox = New System.Windows.Forms.TextBox()
        Me.ProcentajeImpuesto2TextBox = New System.Windows.Forms.TextBox()
        Me.CreditosFiscalesHijoTextBox = New System.Windows.Forms.TextBox()
        Me.CreditosFiscalesConyugeTextBox = New System.Windows.Forms.TextBox()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ArchivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegresarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.AgregarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        CodImpuestoLabel = New System.Windows.Forms.Label()
        ProcentajeImpuesto1Label = New System.Windows.Forms.Label()
        ProcentajeImpuesto2Label = New System.Windows.Forms.Label()
        CreditosFiscalesHijoLabel = New System.Windows.Forms.Label()
        CreditosFiscalesConyugeLabel = New System.Windows.Forms.Label()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImpuestosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CodImpuestoLabel
        '
        CodImpuestoLabel.AutoSize = True
        CodImpuestoLabel.Location = New System.Drawing.Point(83, 59)
        CodImpuestoLabel.Name = "CodImpuestoLabel"
        CodImpuestoLabel.Size = New System.Drawing.Size(74, 13)
        CodImpuestoLabel.TabIndex = 1
        CodImpuestoLabel.Text = "cod Impuesto:"
        '
        'ProcentajeImpuesto1Label
        '
        ProcentajeImpuesto1Label.AutoSize = True
        ProcentajeImpuesto1Label.Location = New System.Drawing.Point(83, 85)
        ProcentajeImpuesto1Label.Name = "ProcentajeImpuesto1Label"
        ProcentajeImpuesto1Label.Size = New System.Drawing.Size(112, 13)
        ProcentajeImpuesto1Label.TabIndex = 3
        ProcentajeImpuesto1Label.Text = "procentaje Impuesto1:"
        '
        'ProcentajeImpuesto2Label
        '
        ProcentajeImpuesto2Label.AutoSize = True
        ProcentajeImpuesto2Label.Location = New System.Drawing.Point(83, 111)
        ProcentajeImpuesto2Label.Name = "ProcentajeImpuesto2Label"
        ProcentajeImpuesto2Label.Size = New System.Drawing.Size(112, 13)
        ProcentajeImpuesto2Label.TabIndex = 5
        ProcentajeImpuesto2Label.Text = "procentaje Impuesto2:"
        '
        'CreditosFiscalesHijoLabel
        '
        CreditosFiscalesHijoLabel.AutoSize = True
        CreditosFiscalesHijoLabel.Location = New System.Drawing.Point(83, 137)
        CreditosFiscalesHijoLabel.Name = "CreditosFiscalesHijoLabel"
        CreditosFiscalesHijoLabel.Size = New System.Drawing.Size(109, 13)
        CreditosFiscalesHijoLabel.TabIndex = 7
        CreditosFiscalesHijoLabel.Text = "creditos Fiscales Hijo:"
        '
        'CreditosFiscalesConyugeLabel
        '
        CreditosFiscalesConyugeLabel.AutoSize = True
        CreditosFiscalesConyugeLabel.Location = New System.Drawing.Point(83, 163)
        CreditosFiscalesConyugeLabel.Name = "CreditosFiscalesConyugeLabel"
        CreditosFiscalesConyugeLabel.Size = New System.Drawing.Size(133, 13)
        CreditosFiscalesConyugeLabel.TabIndex = 9
        CreditosFiscalesConyugeLabel.Text = "creditos Fiscales Conyuge:"
        '
        'Planilla2DataSet
        '
        Me.Planilla2DataSet.DataSetName = "Planilla2DataSet"
        Me.Planilla2DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ImpuestosBindingSource
        '
        Me.ImpuestosBindingSource.DataMember = "Impuestos"
        Me.ImpuestosBindingSource.DataSource = Me.Planilla2DataSet
        '
        'ImpuestosTableAdapter
        '
        Me.ImpuestosTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AjustesTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DatosPersonalesTableAdapter = Nothing
        Me.TableAdapterManager.DeduccionesTableAdapter = Nothing
        Me.TableAdapterManager.ImpuestosTableAdapter = Me.ImpuestosTableAdapter
        Me.TableAdapterManager.PlanillaBisemanalTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaMensualTableAdapter = Nothing
        Me.TableAdapterManager.PuestosTableAdapter = Nothing
        Me.TableAdapterManager.RegistroUsuarioTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'CodImpuestoTextBox
        '
        Me.CodImpuestoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "codImpuesto", True))
        Me.CodImpuestoTextBox.Location = New System.Drawing.Point(222, 56)
        Me.CodImpuestoTextBox.Name = "CodImpuestoTextBox"
        Me.CodImpuestoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CodImpuestoTextBox.TabIndex = 2
        '
        'ProcentajeImpuesto1TextBox
        '
        Me.ProcentajeImpuesto1TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "procentajeImpuesto1", True))
        Me.ProcentajeImpuesto1TextBox.Location = New System.Drawing.Point(222, 82)
        Me.ProcentajeImpuesto1TextBox.Name = "ProcentajeImpuesto1TextBox"
        Me.ProcentajeImpuesto1TextBox.Size = New System.Drawing.Size(100, 20)
        Me.ProcentajeImpuesto1TextBox.TabIndex = 4
        '
        'ProcentajeImpuesto2TextBox
        '
        Me.ProcentajeImpuesto2TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "procentajeImpuesto2", True))
        Me.ProcentajeImpuesto2TextBox.Location = New System.Drawing.Point(222, 108)
        Me.ProcentajeImpuesto2TextBox.Name = "ProcentajeImpuesto2TextBox"
        Me.ProcentajeImpuesto2TextBox.Size = New System.Drawing.Size(100, 20)
        Me.ProcentajeImpuesto2TextBox.TabIndex = 6
        '
        'CreditosFiscalesHijoTextBox
        '
        Me.CreditosFiscalesHijoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "creditosFiscalesHijo", True))
        Me.CreditosFiscalesHijoTextBox.Location = New System.Drawing.Point(222, 134)
        Me.CreditosFiscalesHijoTextBox.Name = "CreditosFiscalesHijoTextBox"
        Me.CreditosFiscalesHijoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CreditosFiscalesHijoTextBox.TabIndex = 8
        '
        'CreditosFiscalesConyugeTextBox
        '
        Me.CreditosFiscalesConyugeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ImpuestosBindingSource, "creditosFiscalesConyuge", True))
        Me.CreditosFiscalesConyugeTextBox.Location = New System.Drawing.Point(222, 160)
        Me.CreditosFiscalesConyugeTextBox.Name = "CreditosFiscalesConyugeTextBox"
        Me.CreditosFiscalesConyugeTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CreditosFiscalesConyugeTextBox.TabIndex = 10
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(85, 205)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(75, 23)
        Me.btnAgregar.TabIndex = 11
        Me.btnAgregar.Text = "&Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(246, 205)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 12
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(327, 205)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 13
        Me.btnEliminar.Text = "&Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArchivoToolStripMenuItem, Me.ConsultarToolStripMenuItem, Me.AgregarToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(505, 24)
        Me.MenuStrip1.TabIndex = 14
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ArchivoToolStripMenuItem
        '
        Me.ArchivoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RegresarToolStripMenuItem})
        Me.ArchivoToolStripMenuItem.Name = "ArchivoToolStripMenuItem"
        Me.ArchivoToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.ArchivoToolStripMenuItem.Text = "&Archivo"
        '
        'RegresarToolStripMenuItem
        '
        Me.RegresarToolStripMenuItem.Name = "RegresarToolStripMenuItem"
        Me.RegresarToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.RegresarToolStripMenuItem.Text = "&Regresar"
        '
        'ConsultarToolStripMenuItem
        '
        Me.ConsultarToolStripMenuItem.Name = "ConsultarToolStripMenuItem"
        Me.ConsultarToolStripMenuItem.Size = New System.Drawing.Size(70, 20)
        Me.ConsultarToolStripMenuItem.Text = "&Consultar"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 270)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(505, 22)
        Me.StatusStrip1.TabIndex = 15
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(166, 205)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(75, 23)
        Me.btnBuscar.TabIndex = 16
        Me.btnBuscar.Text = "&Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'AgregarToolStripMenuItem
        '
        Me.AgregarToolStripMenuItem.Name = "AgregarToolStripMenuItem"
        Me.AgregarToolStripMenuItem.Size = New System.Drawing.Size(100, 20)
        Me.AgregarToolStripMenuItem.Text = "&Nuevo Registro"
        '
        'frmImpuesto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(505, 292)
        Me.Controls.Add(Me.btnBuscar)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(CodImpuestoLabel)
        Me.Controls.Add(Me.CodImpuestoTextBox)
        Me.Controls.Add(ProcentajeImpuesto1Label)
        Me.Controls.Add(Me.ProcentajeImpuesto1TextBox)
        Me.Controls.Add(ProcentajeImpuesto2Label)
        Me.Controls.Add(Me.ProcentajeImpuesto2TextBox)
        Me.Controls.Add(CreditosFiscalesHijoLabel)
        Me.Controls.Add(Me.CreditosFiscalesHijoTextBox)
        Me.Controls.Add(CreditosFiscalesConyugeLabel)
        Me.Controls.Add(Me.CreditosFiscalesConyugeTextBox)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmImpuesto"
        Me.Text = "frmImpuesto"
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImpuestosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Planilla2DataSet As PlanillasProyecto3.Planilla2DataSet
    Friend WithEvents ImpuestosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ImpuestosTableAdapter As PlanillasProyecto3.Planilla2DataSetTableAdapters.ImpuestosTableAdapter
    Friend WithEvents TableAdapterManager As PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager
    Friend WithEvents CodImpuestoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ProcentajeImpuesto1TextBox As System.Windows.Forms.TextBox
    Friend WithEvents ProcentajeImpuesto2TextBox As System.Windows.Forms.TextBox
    Friend WithEvents CreditosFiscalesHijoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CreditosFiscalesConyugeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ArchivoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RegresarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents AgregarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
