﻿Public Class frmImpuesto
    Private valor As Integer
   
    Private Sub frmImpuesto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Planilla2DataSet.Impuestos' table. You can move, or remove it, as needed.
        Me.ImpuestosTableAdapter.Fill(Me.Planilla2DataSet.Impuestos)
        limpiarTexto()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click

        StatusStrip1.Text = "Agregar"

        Try

            Me.ImpuestosTableAdapter.InsertQuery(Integer.Parse(CodImpuestoTextBox.Text), Integer.Parse(ProcentajeImpuesto1TextBox.Text), Integer.Parse(ProcentajeImpuesto2TextBox.Text), Integer.Parse(CreditosFiscalesHijoTextBox.Text), Integer.Parse(CreditosFiscalesConyugeTextBox.Text))
            MsgBox("Datos almacenados exitosamente")
            limpiarTexto()
        Catch ex As FormatException
            MsgBox("Verifique los datos" + ex.Message)
        Catch ex As Exception
            MsgBox("Error" + ex.Message)
        End Try

    End Sub

    Private Sub ConsultarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarToolStripMenuItem.Click
        frmConsultarImpuesto.Show()
        StatusStrip1.Text = "Consultar"
    End Sub

  
    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click

        If MsgBox("Desea actualizar los datos", vbYesNo) = vbYes Then
            Me.Validate()
            Me.ImpuestosBindingSource.EndEdit()
            Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)
            MsgBox("Datos actualizados exitosamente")
        limpiarTexto()
        End If

    End Sub



    Sub limpiarTexto()
        CodImpuestoTextBox.Text = 0
        ProcentajeImpuesto1TextBox.Clear()
        ProcentajeImpuesto2TextBox.Clear()
        CreditosFiscalesHijoTextBox.Clear()
        CreditosFiscalesConyugeTextBox.Clear()
    End Sub



    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        StatusStrip1.Text = "Buscar"
        Try
            valor = InputBox("Ingrese el año a buscar")

            If Me.ImpuestosTableAdapter.FillBy(Me.Planilla2DataSet.Impuestos, Integer.Parse(valor)) = True Then
            Else
                MsgBox("Registro no encontrado")
            End If

           Catch ex As Exception
            MsgBox("Error:" + ex.Message)
        End Try

    End Sub

    Private Sub AgregarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AgregarToolStripMenuItem.Click
        StatusStrip1.Text = "Nuevo Registro"
        limpiarTexto()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Try
            valor = InputBox("Ingrese el año a eliminar")
            If MsgBox("Seguro que desea eliminar el registro", vbYesNo) = vbYes Then
                Me.ImpuestosTableAdapter.DeleteQuery(Integer.Parse(valor))
                MsgBox("Registro eliminado")
            Else
                MsgBox("Registro no encontrado")
            End If
        Catch ex As Exception
            MsgBox("Error" + ex.Message)
        End Try
      
    End Sub

    Private Sub RegresarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegresarToolStripMenuItem.Click
        StatusStrip1.Text = "Regresar"
        frmPrincipal.Show()
        Me.Close()

    End Sub

    Private Sub ArchivoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ArchivoToolStripMenuItem.Click
        StatusStrip1.Text = "Archivo"
    End Sub
End Class