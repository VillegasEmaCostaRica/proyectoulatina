﻿Public Class frmConsultarImpuesto

    Private Sub ImpuestosBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.ImpuestosBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub frmConsultarImpuesto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Planilla2DataSet.Impuestos' table. You can move, or remove it, as needed.
        Me.ImpuestosTableAdapter.Fill(Me.Planilla2DataSet.Impuestos)

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Try
            Me.ImpuestosTableAdapter.FillBy(Me.Planilla2DataSet.Impuestos, Integer.Parse(TextBox1.Text))
        Catch ex As Exception
            MsgBox("Error:" + ex.Message)
        End Try

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click

        Try
            If MsgBox("Seguro que desea eliminar el registro", vbYesNo) = vbYes Then
                Me.ImpuestosTableAdapter.DeleteQuery(Integer.Parse(TextBox1.Text))
                Me.ImpuestosTableAdapter.Fill(Me.Planilla2DataSet.Impuestos)
                MsgBox("Registro eliminado")
            End If
        Catch ex As Exception
            MsgBox("Error" + ex.Message)
        End Try

    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        If MsgBox("Desea actualizar los datos", vbYesNo) = vbYes Then
            Me.Validate()
            Me.ImpuestosBindingSource.EndEdit()
            Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)
            MsgBox("Datos actualizados exitosamente")
        End If
    End Sub

    Private Sub btnMostar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMostar.Click
        Me.ImpuestosTableAdapter.Fill(Me.Planilla2DataSet.Impuestos)
    End Sub
End Class