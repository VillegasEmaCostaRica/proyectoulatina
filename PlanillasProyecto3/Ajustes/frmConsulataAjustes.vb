﻿Public Class frmConsulataAjustes

    Private Sub AjustesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.AjustesBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub frmConsulataAjustes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Planilla2DataSet.DatosPersonales' table. You can move, or remove it, as needed.
        Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet.DatosPersonales)
        'TODO: This line of code loads data into the 'Planilla2DataSet.Ajustes' table. You can move, or remove it, as needed.
        Me.AjustesTableAdapter.Fill(Me.Planilla2DataSet.Ajustes)

    End Sub
End Class