﻿Module ModulePlanilla
    Public datocedula As Integer
    Public nombreCompleto As String
    Public tipoFormularioDatosPersonalesValidar As String
    Public tipoFormularioPuestoValidar As String
    Public empleadoConfianza As Boolean
    Public departamento As String
    Public codPuesto As Integer
    Public usuarioIngresado As String = formUsuario.txtUsuario.Text
    Public contrasenaIngresada As String = formUsuario.txtContrasena.Text
    Public contra As String
    Public usuario As String
    Public tipoOpcionDigitada As String


    Sub TipoDeFormularioDatosResonales(ByVal NomFormulario As String)


        Select Case NomFormulario

            Case "Actualizar"
                frmDatosEmpleadoConsulta.Show()
                tipoFormularioDatosPersonalesValidar = "Actualizar"

            Case "Eliminar"
                frmDatosEmpleadoConsulta.Show()
                tipoFormularioDatosPersonalesValidar = "Eliminar"

            Case Else
        End Select

    End Sub



    Sub tipoDeFormularioPuesto(ByVal NomFormulario As String)


        Select Case NomFormulario
            Case "Actualizar"
                frmPuestosActualizar.Hide()
                frmPuestosConsulta.Show()
                tipoFormularioPuestoValidar = "Actualizar"
            Case "Eliminar"
                frmPuestoEliminar.Hide()
                frmPuestosConsulta.Show()
                tipoFormularioPuestoValidar = "Eliminar"
            Case Else

        End Select


    End Sub



    Sub OperacionUsuario(ByVal TipoOpcion As String)


        Select Case TipoOpcion

            Case "Ingresar"
                frmPrincipal.Hide()
                formOpciones_Usuario.Show()
                tipoOpcionDigitada = "Ingresar"

            Case "Actualizar"
                frmPrincipal.Hide()
                formOpciones_Usuario.txtUsuario.Text = usuarioIngresado
                formOpciones_Usuario.txtContrasena.Text = contrasenaIngresada
                formOpciones_Usuario.Show()
                tipoOpcionDigitada = "Actualizar"

            Case "Eliminar"
                frmPrincipal.Hide()
                formOpciones_Usuario.Show()
                tipoOpcionDigitada = "Eliminar"

            Case Else

        End Select


    End Sub



End Module