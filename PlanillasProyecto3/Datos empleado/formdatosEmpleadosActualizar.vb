﻿Public Class formdatosEmpleadosActualizar
    Dim cedula As Integer

    Private Sub DatosPersonalesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.DatosPersonalesBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub formdatosEmpleadosActualizar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Planilla2DataSet.DatosPersonales' table. You can move, or remove it, as needed.
        ' Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet.DatosPersonales)



    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Try
            Me.DatosPersonalesTableAdapter.FillBy1BuscarPersona(Me.Planilla2DataSet.DatosPersonales, Integer.Parse(txtDatoConsulta.Text))
        Catch ex As Exception
            MsgBox("Verifique los parametros de busqueda: " + ex.Message)
        End Try
    End Sub

    
    Private Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
        Me.Hide()
        TipoDeFormularioDatosResonales("Actualizar")

    End Sub

    Private Sub Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizar.Click

        nombreCompleto = _1__APELLIDOTextBox.Text & " " & _2__APELLIDOTextBox.Text & " " & NOMBRETextBox.Text
        Try

            Me.DatosPersonalesTableAdapter.UpdateDatosPersona(Integer.Parse(NoCEDULATextBox.Text), _1__APELLIDOTextBox.Text, _2__APELLIDOTextBox.Text, NOMBRETextBox.Text, nombreCompleto, E_MAILTextBox.Text, Integer.Parse(CODPUESTOTextBox.Text), FECHA_INGRESODateTimePicker.Text, 0, False, Integer.Parse(N__HIJOSTextBox.Text), GRADO_ACADEMICOTextBox.Text, BANCO_A_DEPOSITARTextBox.Text, NoCUENTATextBox.Text, TelefonoTextBox.Text, CelularTextBox.Text, DireccionTextBox.Text, datocedula)
            Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet.DatosPersonales)
            MsgBox("Se Guardo Sastifactoriamente Los Datos De la Persona")
        Catch ex As Exception
            MsgBox("No se Puede Guardar Los Datos Porque la cedula Coincide Con una cedula De una persona Registrada")
        End Try
    End Sub
End Class