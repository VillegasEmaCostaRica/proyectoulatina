﻿Public Class frmDatosEmpleadoEliminar

    
    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click


        Try
            Me.DatosPersonalesTableAdapter.FillBy1BuscarPersona(Me.Planilla2DataSet.DatosPersonales, Integer.Parse(txtDatoConsulta.Text))

            If MsgBox("¿ESTÁ SEGURO DE ELIMINAR ESTE REGISTRO?", MsgBoxStyle.OkCancel, "CONFIRMAR") = MsgBoxResult.Ok Then

                datocedula = Integer.Parse(txtDatoConsulta.Text)
                Dim lleno As Integer = Me.DatosPersonalesTableAdapter.FillBy1BuscarSiEstaPersona(Me.Planilla2DataSet.DatosPersonales, datocedula)

                If lleno <> 0 Then
                    Me.DatosPersonalesTableAdapter.DeletePersona(datocedula)
                    MsgBox("Se Elimino satisfactoriamente La persona")
                    Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet.DatosPersonales)
                Else
                    MsgBox("No se Ecuentra Ninguana Cedula Guarda, Compruebe En el Boton De Consulta")
                End If
            End If
        Catch ex As Exception
            MsgBox(" Verifique Que la Cedula sea Númerica")
        End Try

        'Me.DatosPersonalesTableAdapter.DeletePersona(Integer.Parse(txtDatoConsulta.Text))
    End Sub

    Private Sub DatosPersonalesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.DatosPersonalesBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub frmDatosEmpleadoEliminar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet.DatosPersonales' Puede moverla o quitarla según sea necesario.
        'Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet.DatosPersonales)

    End Sub

    Private Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
        Me.Hide()
        TipoDeFormularioDatosResonales("Eliminar")

    End Sub
End Class