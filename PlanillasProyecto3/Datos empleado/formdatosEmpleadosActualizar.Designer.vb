﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class formdatosEmpleadosActualizar
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DireccionLabel As System.Windows.Forms.Label
        Dim CelularLabel As System.Windows.Forms.Label
        Dim TelefonoLabel As System.Windows.Forms.Label
        Dim NoCUENTALabel As System.Windows.Forms.Label
        Dim BANCO_A_DEPOSITARLabel As System.Windows.Forms.Label
        Dim GRADO_ACADEMICOLabel As System.Windows.Forms.Label
        Dim N__HIJOSLabel As System.Windows.Forms.Label
        Dim N__CONYUGESLabel As System.Windows.Forms.Label
        Dim AÑOSSERVLabel As System.Windows.Forms.Label
        Dim FECHA_INGRESOLabel As System.Windows.Forms.Label
        Dim CODPUESTOLabel As System.Windows.Forms.Label
        Dim E_MAILLabel As System.Windows.Forms.Label
        Dim NOMBRE_COMPLETOLabel As System.Windows.Forms.Label
        Dim NOMBRELabel As System.Windows.Forms.Label
        Dim _2__APELLIDOLabel As System.Windows.Forms.Label
        Dim _1__APELLIDOLabel As System.Windows.Forms.Label
        Dim NoCEDULALabel As System.Windows.Forms.Label
        Me.Planilla2DataSet = New PlanillasProyecto3.Planilla2DataSet()
        Me.DatosPersonalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DatosPersonalesTableAdapter = New PlanillasProyecto3.Planilla2DataSetTableAdapters.DatosPersonalesTableAdapter()
        Me.TableAdapterManager = New PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtDatoConsulta = New System.Windows.Forms.TextBox()
        Me.DireccionTextBox = New System.Windows.Forms.TextBox()
        Me.CelularTextBox = New System.Windows.Forms.TextBox()
        Me.TelefonoTextBox = New System.Windows.Forms.TextBox()
        Me.NoCUENTATextBox = New System.Windows.Forms.TextBox()
        Me.BANCO_A_DEPOSITARTextBox = New System.Windows.Forms.TextBox()
        Me.GRADO_ACADEMICOTextBox = New System.Windows.Forms.TextBox()
        Me.N__HIJOSTextBox = New System.Windows.Forms.TextBox()
        Me.N__CONYUGESCheckBox = New System.Windows.Forms.CheckBox()
        Me.AÑOSSERVTextBox = New System.Windows.Forms.TextBox()
        Me.FECHA_INGRESODateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.CODPUESTOTextBox = New System.Windows.Forms.TextBox()
        Me.E_MAILTextBox = New System.Windows.Forms.TextBox()
        Me.NOMBRE_COMPLETOTextBox = New System.Windows.Forms.TextBox()
        Me.NOMBRETextBox = New System.Windows.Forms.TextBox()
        Me._2__APELLIDOTextBox = New System.Windows.Forms.TextBox()
        Me._1__APELLIDOTextBox = New System.Windows.Forms.TextBox()
        Me.NoCEDULATextBox = New System.Windows.Forms.TextBox()
        Me.btnConsultar = New System.Windows.Forms.Button()
        Me.btnActualizar = New System.Windows.Forms.Button()
        DireccionLabel = New System.Windows.Forms.Label()
        CelularLabel = New System.Windows.Forms.Label()
        TelefonoLabel = New System.Windows.Forms.Label()
        NoCUENTALabel = New System.Windows.Forms.Label()
        BANCO_A_DEPOSITARLabel = New System.Windows.Forms.Label()
        GRADO_ACADEMICOLabel = New System.Windows.Forms.Label()
        N__HIJOSLabel = New System.Windows.Forms.Label()
        N__CONYUGESLabel = New System.Windows.Forms.Label()
        AÑOSSERVLabel = New System.Windows.Forms.Label()
        FECHA_INGRESOLabel = New System.Windows.Forms.Label()
        CODPUESTOLabel = New System.Windows.Forms.Label()
        E_MAILLabel = New System.Windows.Forms.Label()
        NOMBRE_COMPLETOLabel = New System.Windows.Forms.Label()
        NOMBRELabel = New System.Windows.Forms.Label()
        _2__APELLIDOLabel = New System.Windows.Forms.Label()
        _1__APELLIDOLabel = New System.Windows.Forms.Label()
        NoCEDULALabel = New System.Windows.Forms.Label()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DatosPersonalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DireccionLabel
        '
        DireccionLabel.AutoSize = True
        DireccionLabel.Location = New System.Drawing.Point(47, 521)
        DireccionLabel.Name = "DireccionLabel"
        DireccionLabel.Size = New System.Drawing.Size(55, 13)
        DireccionLabel.TabIndex = 36
        DireccionLabel.Text = "Direccion:"
        '
        'CelularLabel
        '
        CelularLabel.AutoSize = True
        CelularLabel.Location = New System.Drawing.Point(47, 490)
        CelularLabel.Name = "CelularLabel"
        CelularLabel.Size = New System.Drawing.Size(42, 13)
        CelularLabel.TabIndex = 34
        CelularLabel.Text = "Celular:"
        '
        'TelefonoLabel
        '
        TelefonoLabel.AutoSize = True
        TelefonoLabel.Location = New System.Drawing.Point(47, 464)
        TelefonoLabel.Name = "TelefonoLabel"
        TelefonoLabel.Size = New System.Drawing.Size(52, 13)
        TelefonoLabel.TabIndex = 32
        TelefonoLabel.Text = "Telefono:"
        '
        'NoCUENTALabel
        '
        NoCUENTALabel.AutoSize = True
        NoCUENTALabel.Location = New System.Drawing.Point(47, 438)
        NoCUENTALabel.Name = "NoCUENTALabel"
        NoCUENTALabel.Size = New System.Drawing.Size(71, 13)
        NoCUENTALabel.TabIndex = 30
        NoCUENTALabel.Text = "No CUENTA:"
        '
        'BANCO_A_DEPOSITARLabel
        '
        BANCO_A_DEPOSITARLabel.AutoSize = True
        BANCO_A_DEPOSITARLabel.Location = New System.Drawing.Point(47, 412)
        BANCO_A_DEPOSITARLabel.Name = "BANCO_A_DEPOSITARLabel"
        BANCO_A_DEPOSITARLabel.Size = New System.Drawing.Size(122, 13)
        BANCO_A_DEPOSITARLabel.TabIndex = 28
        BANCO_A_DEPOSITARLabel.Text = "BANCO A DEPOSITAR:"
        '
        'GRADO_ACADEMICOLabel
        '
        GRADO_ACADEMICOLabel.AutoSize = True
        GRADO_ACADEMICOLabel.Location = New System.Drawing.Point(47, 386)
        GRADO_ACADEMICOLabel.Name = "GRADO_ACADEMICOLabel"
        GRADO_ACADEMICOLabel.Size = New System.Drawing.Size(115, 13)
        GRADO_ACADEMICOLabel.TabIndex = 26
        GRADO_ACADEMICOLabel.Text = "GRADO ACADEMICO:"
        '
        'N__HIJOSLabel
        '
        N__HIJOSLabel.AutoSize = True
        N__HIJOSLabel.Location = New System.Drawing.Point(47, 360)
        N__HIJOSLabel.Name = "N__HIJOSLabel"
        N__HIJOSLabel.Size = New System.Drawing.Size(56, 13)
        N__HIJOSLabel.TabIndex = 24
        N__HIJOSLabel.Text = "N° HIJOS:"
        '
        'N__CONYUGESLabel
        '
        N__CONYUGESLabel.AutoSize = True
        N__CONYUGESLabel.Location = New System.Drawing.Point(47, 332)
        N__CONYUGESLabel.Name = "N__CONYUGESLabel"
        N__CONYUGESLabel.Size = New System.Drawing.Size(54, 13)
        N__CONYUGESLabel.TabIndex = 22
        N__CONYUGESLabel.Text = "CASADO:"
        '
        'AÑOSSERVLabel
        '
        AÑOSSERVLabel.AutoSize = True
        AÑOSSERVLabel.Location = New System.Drawing.Point(47, 304)
        AÑOSSERVLabel.Name = "AÑOSSERVLabel"
        AÑOSSERVLabel.Size = New System.Drawing.Size(69, 13)
        AÑOSSERVLabel.TabIndex = 20
        AÑOSSERVLabel.Text = "AÑOSSERV:"
        '
        'FECHA_INGRESOLabel
        '
        FECHA_INGRESOLabel.AutoSize = True
        FECHA_INGRESOLabel.Location = New System.Drawing.Point(47, 279)
        FECHA_INGRESOLabel.Name = "FECHA_INGRESOLabel"
        FECHA_INGRESOLabel.Size = New System.Drawing.Size(97, 13)
        FECHA_INGRESOLabel.TabIndex = 18
        FECHA_INGRESOLabel.Text = "FECHA INGRESO:"
        '
        'CODPUESTOLabel
        '
        CODPUESTOLabel.AutoSize = True
        CODPUESTOLabel.Location = New System.Drawing.Point(47, 252)
        CODPUESTOLabel.Name = "CODPUESTOLabel"
        CODPUESTOLabel.Size = New System.Drawing.Size(77, 13)
        CODPUESTOLabel.TabIndex = 16
        CODPUESTOLabel.Text = "CODPUESTO:"
        '
        'E_MAILLabel
        '
        E_MAILLabel.AutoSize = True
        E_MAILLabel.Location = New System.Drawing.Point(47, 226)
        E_MAILLabel.Name = "E_MAILLabel"
        E_MAILLabel.Size = New System.Drawing.Size(45, 13)
        E_MAILLabel.TabIndex = 14
        E_MAILLabel.Text = "E-MAIL:"
        '
        'NOMBRE_COMPLETOLabel
        '
        NOMBRE_COMPLETOLabel.AutoSize = True
        NOMBRE_COMPLETOLabel.Location = New System.Drawing.Point(47, 200)
        NOMBRE_COMPLETOLabel.Name = "NOMBRE_COMPLETOLabel"
        NOMBRE_COMPLETOLabel.Size = New System.Drawing.Size(119, 13)
        NOMBRE_COMPLETOLabel.TabIndex = 12
        NOMBRE_COMPLETOLabel.Text = "NOMBRE COMPLETO:"
        '
        'NOMBRELabel
        '
        NOMBRELabel.AutoSize = True
        NOMBRELabel.Location = New System.Drawing.Point(47, 174)
        NOMBRELabel.Name = "NOMBRELabel"
        NOMBRELabel.Size = New System.Drawing.Size(57, 13)
        NOMBRELabel.TabIndex = 10
        NOMBRELabel.Text = "NOMBRE:"
        '
        '_2__APELLIDOLabel
        '
        _2__APELLIDOLabel.AutoSize = True
        _2__APELLIDOLabel.Location = New System.Drawing.Point(47, 148)
        _2__APELLIDOLabel.Name = "_2__APELLIDOLabel"
        _2__APELLIDOLabel.Size = New System.Drawing.Size(75, 13)
        _2__APELLIDOLabel.TabIndex = 8
        _2__APELLIDOLabel.Text = "2° APELLIDO:"
        '
        '_1__APELLIDOLabel
        '
        _1__APELLIDOLabel.AutoSize = True
        _1__APELLIDOLabel.Location = New System.Drawing.Point(47, 122)
        _1__APELLIDOLabel.Name = "_1__APELLIDOLabel"
        _1__APELLIDOLabel.Size = New System.Drawing.Size(75, 13)
        _1__APELLIDOLabel.TabIndex = 6
        _1__APELLIDOLabel.Text = "1° APELLIDO:"
        '
        'NoCEDULALabel
        '
        NoCEDULALabel.AutoSize = True
        NoCEDULALabel.Location = New System.Drawing.Point(47, 96)
        NoCEDULALabel.Name = "NoCEDULALabel"
        NoCEDULALabel.Size = New System.Drawing.Size(70, 13)
        NoCEDULALabel.TabIndex = 4
        NoCEDULALabel.Text = "No CEDULA:"
        '
        'Planilla2DataSet
        '
        Me.Planilla2DataSet.DataSetName = "Planilla2DataSet"
        Me.Planilla2DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DatosPersonalesBindingSource
        '
        Me.DatosPersonalesBindingSource.DataMember = "DatosPersonales"
        Me.DatosPersonalesBindingSource.DataSource = Me.Planilla2DataSet
        '
        'DatosPersonalesTableAdapter
        '
        Me.DatosPersonalesTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AjustesTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DatosPersonalesTableAdapter = Me.DatosPersonalesTableAdapter
        Me.TableAdapterManager.DeduccionesTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaBisemanalTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaMensualTableAdapter = Nothing
        Me.TableAdapterManager.PuestosTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(340, 20)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(75, 22)
        Me.btnBuscar.TabIndex = 0
        Me.btnBuscar.Text = "&Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(44, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(147, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Cedula Que desea Actualizar:"
        '
        'txtDatoConsulta
        '
        Me.txtDatoConsulta.Location = New System.Drawing.Point(215, 22)
        Me.txtDatoConsulta.Name = "txtDatoConsulta"
        Me.txtDatoConsulta.Size = New System.Drawing.Size(108, 20)
        Me.txtDatoConsulta.TabIndex = 2
        '
        'DireccionTextBox
        '
        Me.DireccionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "Direccion", True))
        Me.DireccionTextBox.Location = New System.Drawing.Point(215, 518)
        Me.DireccionTextBox.Name = "DireccionTextBox"
        Me.DireccionTextBox.Size = New System.Drawing.Size(200, 20)
        Me.DireccionTextBox.TabIndex = 37
        '
        'CelularTextBox
        '
        Me.CelularTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "Celular", True))
        Me.CelularTextBox.Location = New System.Drawing.Point(215, 487)
        Me.CelularTextBox.Name = "CelularTextBox"
        Me.CelularTextBox.Size = New System.Drawing.Size(200, 20)
        Me.CelularTextBox.TabIndex = 35
        '
        'TelefonoTextBox
        '
        Me.TelefonoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "Telefono", True))
        Me.TelefonoTextBox.Location = New System.Drawing.Point(215, 461)
        Me.TelefonoTextBox.Name = "TelefonoTextBox"
        Me.TelefonoTextBox.Size = New System.Drawing.Size(200, 20)
        Me.TelefonoTextBox.TabIndex = 33
        '
        'NoCUENTATextBox
        '
        Me.NoCUENTATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "NoCUENTA", True))
        Me.NoCUENTATextBox.Location = New System.Drawing.Point(215, 435)
        Me.NoCUENTATextBox.Name = "NoCUENTATextBox"
        Me.NoCUENTATextBox.Size = New System.Drawing.Size(200, 20)
        Me.NoCUENTATextBox.TabIndex = 31
        '
        'BANCO_A_DEPOSITARTextBox
        '
        Me.BANCO_A_DEPOSITARTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "BANCO A DEPOSITAR", True))
        Me.BANCO_A_DEPOSITARTextBox.Location = New System.Drawing.Point(215, 409)
        Me.BANCO_A_DEPOSITARTextBox.Name = "BANCO_A_DEPOSITARTextBox"
        Me.BANCO_A_DEPOSITARTextBox.Size = New System.Drawing.Size(200, 20)
        Me.BANCO_A_DEPOSITARTextBox.TabIndex = 29
        '
        'GRADO_ACADEMICOTextBox
        '
        Me.GRADO_ACADEMICOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "GRADO ACADEMICO", True))
        Me.GRADO_ACADEMICOTextBox.Location = New System.Drawing.Point(215, 383)
        Me.GRADO_ACADEMICOTextBox.Name = "GRADO_ACADEMICOTextBox"
        Me.GRADO_ACADEMICOTextBox.Size = New System.Drawing.Size(200, 20)
        Me.GRADO_ACADEMICOTextBox.TabIndex = 27
        '
        'N__HIJOSTextBox
        '
        Me.N__HIJOSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "N° HIJOS", True))
        Me.N__HIJOSTextBox.Location = New System.Drawing.Point(215, 357)
        Me.N__HIJOSTextBox.Name = "N__HIJOSTextBox"
        Me.N__HIJOSTextBox.Size = New System.Drawing.Size(200, 20)
        Me.N__HIJOSTextBox.TabIndex = 25
        '
        'N__CONYUGESCheckBox
        '
        Me.N__CONYUGESCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.DatosPersonalesBindingSource, "N° CONYUGES", True))
        Me.N__CONYUGESCheckBox.Location = New System.Drawing.Point(215, 327)
        Me.N__CONYUGESCheckBox.Name = "N__CONYUGESCheckBox"
        Me.N__CONYUGESCheckBox.Size = New System.Drawing.Size(200, 24)
        Me.N__CONYUGESCheckBox.TabIndex = 23
        Me.N__CONYUGESCheckBox.Text = "Si"
        Me.N__CONYUGESCheckBox.UseVisualStyleBackColor = True
        '
        'AÑOSSERVTextBox
        '
        Me.AÑOSSERVTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "AÑOSSERV", True))
        Me.AÑOSSERVTextBox.Location = New System.Drawing.Point(215, 301)
        Me.AÑOSSERVTextBox.Name = "AÑOSSERVTextBox"
        Me.AÑOSSERVTextBox.Size = New System.Drawing.Size(200, 20)
        Me.AÑOSSERVTextBox.TabIndex = 21
        '
        'FECHA_INGRESODateTimePicker
        '
        Me.FECHA_INGRESODateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.DatosPersonalesBindingSource, "FECHA INGRESO", True))
        Me.FECHA_INGRESODateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FECHA_INGRESODateTimePicker.Location = New System.Drawing.Point(215, 275)
        Me.FECHA_INGRESODateTimePicker.Name = "FECHA_INGRESODateTimePicker"
        Me.FECHA_INGRESODateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.FECHA_INGRESODateTimePicker.TabIndex = 19
        '
        'CODPUESTOTextBox
        '
        Me.CODPUESTOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "CODPUESTO", True))
        Me.CODPUESTOTextBox.Location = New System.Drawing.Point(215, 249)
        Me.CODPUESTOTextBox.Name = "CODPUESTOTextBox"
        Me.CODPUESTOTextBox.Size = New System.Drawing.Size(200, 20)
        Me.CODPUESTOTextBox.TabIndex = 17
        '
        'E_MAILTextBox
        '
        Me.E_MAILTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "E-MAIL", True))
        Me.E_MAILTextBox.Location = New System.Drawing.Point(215, 223)
        Me.E_MAILTextBox.Name = "E_MAILTextBox"
        Me.E_MAILTextBox.Size = New System.Drawing.Size(200, 20)
        Me.E_MAILTextBox.TabIndex = 15
        '
        'NOMBRE_COMPLETOTextBox
        '
        Me.NOMBRE_COMPLETOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "NOMBRE COMPLETO", True))
        Me.NOMBRE_COMPLETOTextBox.Location = New System.Drawing.Point(215, 197)
        Me.NOMBRE_COMPLETOTextBox.Name = "NOMBRE_COMPLETOTextBox"
        Me.NOMBRE_COMPLETOTextBox.Size = New System.Drawing.Size(200, 20)
        Me.NOMBRE_COMPLETOTextBox.TabIndex = 13
        '
        'NOMBRETextBox
        '
        Me.NOMBRETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "NOMBRE", True))
        Me.NOMBRETextBox.Location = New System.Drawing.Point(215, 171)
        Me.NOMBRETextBox.Name = "NOMBRETextBox"
        Me.NOMBRETextBox.Size = New System.Drawing.Size(200, 20)
        Me.NOMBRETextBox.TabIndex = 11
        '
        '_2__APELLIDOTextBox
        '
        Me._2__APELLIDOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "2° APELLIDO", True))
        Me._2__APELLIDOTextBox.Location = New System.Drawing.Point(215, 145)
        Me._2__APELLIDOTextBox.Name = "_2__APELLIDOTextBox"
        Me._2__APELLIDOTextBox.Size = New System.Drawing.Size(200, 20)
        Me._2__APELLIDOTextBox.TabIndex = 9
        '
        '_1__APELLIDOTextBox
        '
        Me._1__APELLIDOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "1° APELLIDO", True))
        Me._1__APELLIDOTextBox.Location = New System.Drawing.Point(215, 119)
        Me._1__APELLIDOTextBox.Name = "_1__APELLIDOTextBox"
        Me._1__APELLIDOTextBox.Size = New System.Drawing.Size(200, 20)
        Me._1__APELLIDOTextBox.TabIndex = 7
        '
        'NoCEDULATextBox
        '
        Me.NoCEDULATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "NoCEDULA", True))
        Me.NoCEDULATextBox.Location = New System.Drawing.Point(215, 93)
        Me.NoCEDULATextBox.Name = "NoCEDULATextBox"
        Me.NoCEDULATextBox.Size = New System.Drawing.Size(200, 20)
        Me.NoCEDULATextBox.TabIndex = 5
        '
        'btnConsultar
        '
        Me.btnConsultar.Location = New System.Drawing.Point(488, 20)
        Me.btnConsultar.Name = "btnConsultar"
        Me.btnConsultar.Size = New System.Drawing.Size(121, 23)
        Me.btnConsultar.TabIndex = 38
        Me.btnConsultar.Text = "&Consultar Datos"
        Me.btnConsultar.UseVisualStyleBackColor = True
        '
        'btnActualizar
        '
        Me.btnActualizar.Location = New System.Drawing.Point(562, 236)
        Me.btnActualizar.Name = "btnActualizar"
        Me.btnActualizar.Size = New System.Drawing.Size(87, 44)
        Me.btnActualizar.TabIndex = 39
        Me.btnActualizar.Text = "&Guardar Actualización"
        Me.btnActualizar.UseVisualStyleBackColor = True
        '
        'formdatosEmpleadosActualizar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(718, 673)
        Me.Controls.Add(Me.btnActualizar)
        Me.Controls.Add(Me.btnConsultar)
        Me.Controls.Add(NoCEDULALabel)
        Me.Controls.Add(Me.NoCEDULATextBox)
        Me.Controls.Add(_1__APELLIDOLabel)
        Me.Controls.Add(Me._1__APELLIDOTextBox)
        Me.Controls.Add(_2__APELLIDOLabel)
        Me.Controls.Add(Me._2__APELLIDOTextBox)
        Me.Controls.Add(NOMBRELabel)
        Me.Controls.Add(Me.NOMBRETextBox)
        Me.Controls.Add(NOMBRE_COMPLETOLabel)
        Me.Controls.Add(Me.NOMBRE_COMPLETOTextBox)
        Me.Controls.Add(E_MAILLabel)
        Me.Controls.Add(Me.E_MAILTextBox)
        Me.Controls.Add(CODPUESTOLabel)
        Me.Controls.Add(Me.CODPUESTOTextBox)
        Me.Controls.Add(FECHA_INGRESOLabel)
        Me.Controls.Add(Me.FECHA_INGRESODateTimePicker)
        Me.Controls.Add(AÑOSSERVLabel)
        Me.Controls.Add(Me.AÑOSSERVTextBox)
        Me.Controls.Add(N__CONYUGESLabel)
        Me.Controls.Add(Me.N__CONYUGESCheckBox)
        Me.Controls.Add(N__HIJOSLabel)
        Me.Controls.Add(Me.N__HIJOSTextBox)
        Me.Controls.Add(GRADO_ACADEMICOLabel)
        Me.Controls.Add(Me.GRADO_ACADEMICOTextBox)
        Me.Controls.Add(BANCO_A_DEPOSITARLabel)
        Me.Controls.Add(Me.BANCO_A_DEPOSITARTextBox)
        Me.Controls.Add(NoCUENTALabel)
        Me.Controls.Add(Me.NoCUENTATextBox)
        Me.Controls.Add(TelefonoLabel)
        Me.Controls.Add(Me.TelefonoTextBox)
        Me.Controls.Add(CelularLabel)
        Me.Controls.Add(Me.CelularTextBox)
        Me.Controls.Add(DireccionLabel)
        Me.Controls.Add(Me.DireccionTextBox)
        Me.Controls.Add(Me.txtDatoConsulta)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnBuscar)
        Me.Name = "formdatosEmpleadosActualizar"
        Me.Text = "formdatosEmpleadosActualizar"
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DatosPersonalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Planilla2DataSet As PlanillasProyecto3.Planilla2DataSet
    Friend WithEvents DatosPersonalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DatosPersonalesTableAdapter As PlanillasProyecto3.Planilla2DataSetTableAdapters.DatosPersonalesTableAdapter
    Friend WithEvents TableAdapterManager As PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtDatoConsulta As System.Windows.Forms.TextBox
    Friend WithEvents DireccionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CelularTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TelefonoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NoCUENTATextBox As System.Windows.Forms.TextBox
    Friend WithEvents BANCO_A_DEPOSITARTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GRADO_ACADEMICOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents N__HIJOSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents N__CONYUGESCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents AÑOSSERVTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FECHA_INGRESODateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents CODPUESTOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents E_MAILTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NOMBRE_COMPLETOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NOMBRETextBox As System.Windows.Forms.TextBox
    Friend WithEvents _2__APELLIDOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents _1__APELLIDOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NoCEDULATextBox As System.Windows.Forms.TextBox
    Friend WithEvents btnConsultar As System.Windows.Forms.Button
    Friend WithEvents btnActualizar As System.Windows.Forms.Button
End Class
